<?php
/**
 * Template for website search form
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<span class="fa <?php the_field( 'search_form_icon', 'option' ); ?>"></span>
		<input type="search" class="search-field form-control"
			   placeholder="<?php echo esc_attr_x( 'Type a keyword and hit enter', 'placeholder', 'liquormaster' ) ?>"
			   value="<?php echo get_search_query() ?>" name="s"/>
	</div>
</form>

