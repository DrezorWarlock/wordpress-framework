<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$featured_image_id    = $product->get_image_id();
$gallery_image_ids    = $product->get_gallery_image_ids();
$variation_images_ids = array();
$product_image_ids    = array();
$default_attributes   = $product->get_default_attributes();
$default_variation    = '';

if ( ! empty( $default_attributes ) ) {
	$default_attributes_prepared = array();

	foreach ( $default_attributes as $default_attribute_name => $default_attribute_value ) {
		$default_attributes_prepared[ 'attribute_' . $default_attribute_name ] = $default_attribute_value;
	}

	$default_variation    = new WC_Product_Data_Store_CPT();
	$default_variation_id = $default_variation->find_matching_product_variation( $product, $default_attributes_prepared );
}


$default_image = '';

if ( $product->is_type( 'variable' ) && ! empty( $default_variation_id ) ) {
	$default_image = get_the_post_thumbnail_url( $default_variation_id, 'large' );
} else if ( empty( $featured_image_id ) ) {
	$default_image = esc_url( wc_placeholder_img_src( 'woocommerce_single' ) );
} else {
	$default_image = wp_get_attachment_image_url( $featured_image_id, 'large' );
}

if ( $product->is_type( 'variable' ) ) {
	$product_variations = $product->get_available_variations();

	foreach ( $product_variations as $product_variation ) {
		$variation_images_ids[ $product_variation['variation_id'] ] = $product_variation['image_id'];
	}
}

$thumbnails_html = '';

if ( ! empty( $featured_image_id ) || ! empty( $gallery_image_ids ) ) {

	$product_image_ids = array_replace( $product_image_ids, $variation_images_ids, $gallery_image_ids );
	$product_image_ids = array( count( $product_image_ids ) => intval( $featured_image_id ) ) + $product_image_ids;
	$product_image_ids = array_unique( $product_image_ids );

	$thumbnail_urls = '';
	if ( count( $product_image_ids ) > 1 ) {
		foreach ( $product_image_ids as $key => $product_image_id ) {
			$large_image_urls = wp_get_attachment_image_url( $product_image_id, 'large' );
			$thumbnail_urls   = wp_get_attachment_image_url( $product_image_id, 'woocommerce_gallery_thumbnail' );

			$thumbnails_html .= <<<HTML
<div class="thumbnail">
	<img src="{$thumbnail_urls}" data-variation-id="{$key}" data-large-src="{$large_image_urls}" class="img-fluid wp-post-image">
</div>
HTML;
		}
	}
}
?>

<div class="col-lg-6 mb-5 ftco-animate">
	<div class="images" data-default-image="<?php echo $default_image ?>">
		<div class="woocommerce-product-gallery__image">
			<a href="<?php echo $default_image ?>"
			   class="image-popup prod-img-bg wp-post-image">
				<img src="<?php echo $default_image ?>"
					 class="img-fluid"
					 alt="Colorlib Template"/>
			</a>
			<?php if ( ! empty( $thumbnails_html ) ) : ?>
				<div class="product-gallery-thumbnails">
					<?php echo $thumbnails_html ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
