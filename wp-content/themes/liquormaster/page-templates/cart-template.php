<?php
/**
 * Template name: Cart Template
 *
 * Template to render shop's cart
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>
<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				}
				?>
		</div>
	</div>
</section>
<?php
get_footer();
