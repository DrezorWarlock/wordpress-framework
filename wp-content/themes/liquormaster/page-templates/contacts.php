<?php
/**
 * Template name: Contacts
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

init_acf_google_map();
get_header();

$contact_form_id = get_field( 'contact_form_id' );
?>

	<section class="ftco-section bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="wrapper px-md-4">
						<?php if ( have_rows( 'contact_blocks' ) ) : ?>
							<div class="row mb-5">
								<?php while ( have_rows( 'contact_blocks' ) ) : the_row(); ?>
									<div class="col-md-3">
										<div class="dbox w-100 text-center">
											<div class="icon d-flex align-items-center justify-content-center">
												<span class="fa <?php the_sub_field( 'contact_block_icon' ) ?>"></span>
											</div>
											<div class="text">
												<p>
													<span><?php the_sub_field( 'contact_block_label' ) ?></span> <?php the_sub_field( 'contact_block_info' ) ?>
												</p>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
						<div class="row no-gutters">
							<div class="col-md-7">
								<div class="contact-wrap w-100 p-md-5 p-4">

									<?php echo do_shortcode( '[contact-form-7 id="'.$contact_form_id.'" html_id="contactForm" html_class="contactForm"]' ) ?>
								</div>
							</div>
							<div class="col-md-5 order-md-first d-flex align-items-stretch">
								<div id="map" class="acf-map">
									<?php
									if ( ! empty( get_field( 'google_maps_locations' ) ) ):
										foreach ( get_field( 'google_maps_locations' ) as $google_map_location ) :
											?>
											<div class="marker"
												 data-lat="<?php echo esc_attr( $google_map_location['google_map_location']['lat'] ); ?>"
												 data-lng="<?php echo esc_attr( $google_map_location['google_map_location']['lng'] ); ?>">
												<h5 class="address-name">
													<?php echo explode( ',', $google_map_location['google_map_location']['address'] )[0]; ?>
												</h5>
											</div>
										<?php
										endforeach;
									endif;
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php

get_footer();
