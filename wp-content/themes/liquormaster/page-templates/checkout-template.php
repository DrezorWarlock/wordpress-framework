<?php
/**
 * Template name: Checkout Template
 *
 * Template to render shop's checkout page
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>
	<section class="ftco-section ftco-degree-bg">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 ftco-animate">
					<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							the_content();
						}
					}
					?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
