<?php
/**
 * Template name: About us
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();

require_once 'wp-content/themes/liquormaster/inc/template-classes/class-liqmaboutus.php';

$about_us = new LiqmAboutUs();

$about_us->render();

get_footer();
