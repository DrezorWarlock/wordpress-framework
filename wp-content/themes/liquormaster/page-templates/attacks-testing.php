<?php
/**
 * Template Name: Attacks testing
 *
 * Template to test different kind of attacks on the website and make sure they don't work.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$xss_test = ( ! empty( $_GET['testing'] ) ) ? sanitize_text_field( $_GET['testing'] ) : null;
//var_dump( $xss_test );

$mail_test = ( ! empty( $_GET['email-testing'] ) ) ? sanitize_email( $_GET['email-testing'] ) : null;
var_dump( $mail_test );

$sql_test_int = ( ! empty( $_GET['team_id'] ) ) ? intval( $_GET['team_id'] ) : null;
$sql_test_txt = ( ! empty( $_GET['team_member_name'] ) ) ? esc_sql( $_GET['team_member_name'] ) : null;

if ( $sql_test_int ) {
	global $wpdb;
	$test_result = $wpdb->get_results( "SELECT post_title FROM `{$wpdb->prefix}posts` where ID = {$sql_test_int}" );

	var_dump( $test_result );
}

if ( $sql_test_txt ) {
	global $wpdb;
	$test_result = $wpdb->get_results( "SELECT post_name FROM {$wpdb->prefix}posts where post_title = '{$sql_test_txt}'" );

	var_dump( $test_result );
}


get_header();
?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form method='get' style="margin: 10px 0">
					<input type="text" name="testing" value="<?php echo $xss_test; ?>" placeholder="for scripts"/>
					<input type="submit" value="Submit"/>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form method='get' style="margin: 10px 0">
					<input type="text" name="email-testing" value="<?php echo $mail_test; ?>" placeholder="for email"/>
					<input type="submit" value="Submit"/>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form method="get">
					<select name="team_id">
						<option value="169">Stephen Yager</option>
						<option value="167">Aaron Wei</option>
					</select>
					<input type="submit" value="Submit"/>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form method="get">
					<select name="team_member_name">
						<option value="Stephen Yager">Stephen Yager</option>
						<option value="Aaron Wei">Aaron Wei</option>
					</select>
					<input type="submit" value="Submit"/>
				</form>
			</div>
		</div>
	</div>
<?php
get_footer();
