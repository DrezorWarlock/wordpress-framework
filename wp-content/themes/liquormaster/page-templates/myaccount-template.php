<?php
/**
 * Template name: My Account Template
 *
 * Template to render user's account
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>
<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
