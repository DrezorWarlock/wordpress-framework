<?php
/**
 * Template name: Ajax-playground
 *
 * Template for Ajax tests.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link" id="privat-archive-tab" data-toggle="tab" href="#privat-archive" role="tab"
						   aria-controls="privat-exchange"
						   aria-selected="true">
							<?php _ex( 'Privat Bank Exchange Rate Archives', 'ajax-playground', 'liquormaster' ) ?>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="privat-exchange-tab" data-toggle="tab" href="#privat-exchange"
						   role="tab"
						   aria-controls="privat-exchange"
						   aria-selected="true">
							<?php _ex( 'Privat Bank Exchange Rate', 'ajax-playground', 'liquormaster' ) ?>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="covid-tab" data-toggle="tab" href="#covid" role="tab"
						   aria-controls="covid-info"
						   aria-selected="true">
							<?php _ex( 'COVID-19 Info', 'ajax-playground', 'liquormaster' ) ?>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" id="fbi-tab" data-toggle="tab" href="#fbi-wanted" role="tab"
						   aria-controls="fbi-wanted"
						   aria-selected="true">
							<?php _ex( 'FBI Wanted', 'ajax-playground', 'liquormaster' ) ?>
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade" id="privat-archive" role="tabpanel" aria-labelledby="privat-archive-tab">
						<div class="request-controls">
							<div class="date-control">
								<label for="exchange-archive-date">
									<?php _ex( 'Exchange Rate Archive Date', 'ajax-playground', 'liquormaster' ) ?>:
								</label>
								<input type="date" id="exchange-archive-date"
									   min="<?php echo date( 'Y-m-d', strtotime( '-4 years' ) ) ?>"
									   max="<?php echo date( 'Y-m-d', strtotime( 'today' ) ) ?>">
							</div>
							<div class="request-submit-button">
								<button type="button" class="request-button">
									<?php _ex( 'Get Privat Bank Currency Exchange Rates Archive', 'ajax-playground', 'liquormaster' ); ?>
								</button>
							</div>
							<div class="notifications">
								<div class="spinner"></div>
								<div class="notification">
									<?php _ex( 'Choose the date first!', 'ajax-playground', 'liquormaster' ); ?>
								</div>
							</div>
						</div>
						<table class="privat-bank-request-result">
							<thead>
							<tr>
								<th><?php _ex( 'Base Currency', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Currency', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Purchase Rate (National Bank)', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Sale Rate (National Bank)', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Purchase Rate (Privat Bank)', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Sale Rate (Privat Bank)', 'ajax-playground', 'liquormaster' ) ?></th>
							</tr>
							</thead>
							<tbody class="ce-info">

							</tbody>
						</table>
					</div>
					<div class="tab-pane fade" id="privat-exchange" role="tabpanel"
						 aria-labelledby="privat-exchange-tab">
						<div class="request-controls">
							<div class="request-submit-button">
								<button type="button" class="request-button">
									<?php _ex( 'Get Privat Bank Currency Exchange Rates', 'ajax-playground', 'liquormaster' ); ?>
								</button>
							</div>
							<div class="notifications">
								<div class="spinner"></div>
							</div>
						</div>
						<table class="privat-bank-request-result">
							<thead>
							<tr>
								<th><?php _ex( 'Base Currency', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Currency', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Purchase Rate', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Sale Rate', 'ajax-playground', 'liquormaster' ) ?></th>
							</tr>
							</thead>
							<tbody class="ce-info">

							</tbody>
						</table>
					</div>
					<div class="tab-pane fade" id="covid" role="tabpanel" aria-labelledby="covid-tab">
						<div class="request-controls">
							<div class="request-submit-button">
								<button type="button" class="request-button">
									<?php _ex( 'Get List of Countries reported COVID-19', 'ajax-playground', 'liquormaster' ); ?>
								</button>
							</div>
							<select id="covid-countries">
								<option value="false" selected disabled>
									<?php echo _x( 'Choose country', 'country name placeholder', 'liquormaster' ) ?>
								</option>
							</select>
							<div class="notifications">
								<div class="spinner"></div>
								<div class="no-data-response">
									<?php _ex( 'Sorry, there is no data for this country!', 'ajax-playground', 'liquormaster' ); ?>
								</div>
							</div>
						</div>
						<div class="chart-block">
							<canvas id="country-chart" style="width: 100%; height: 500px"></canvas>
						</div>

						<table class="covid-request-result">
							<thead>
							<tr>
								<th><?php _ex( 'Date', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Active', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Confirmed', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Recovered', 'ajax-playground', 'liquormaster' ) ?></th>
								<th><?php _ex( 'Death', 'ajax-playground', 'liquormaster' ) ?></th>
							</tr>
							</thead>
							<tbody class="covid-info">

							</tbody>
						</table>
					</div>
					<div class="tab-pane fade show active" id="fbi-wanted" role="tabpanel" aria-labelledby="covid-tab">
						<?php $post_id = apply_filters( 'wpml_object_id', 531, 'post', true ); ?>
						<a href="<?php echo get_the_permalink( $post_id ); ?>"><?php echo get_the_title( $post_id ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();
