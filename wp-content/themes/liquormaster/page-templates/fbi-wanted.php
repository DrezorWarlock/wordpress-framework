<?php
/**
 * Template name: FBI Wanted
 *
 * Template for FBI Wanted Homework.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="field-offices-list-wrapper">
					<select id="fbi-field-offices">
						<option value="false" selected
								disabled><?php _ex( 'Choose FBI Office', 'fbi-wanted', 'liquormaster' ); ?> </option>
						<option value="albany"><?php _ex( 'Albany', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="albuquerque"><?php _ex( 'Albuquerque', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="anchorage"><?php _ex( 'Anchorage', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="atlanta"><?php _ex( 'Atlanta', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="baltimore"><?php _ex( 'Baltimore', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="birmingham"><?php _ex( 'Birmingham', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="boston"><?php _ex( 'Boston', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="buffalo"><?php _ex( 'Buffalo', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="charlotte"><?php _ex( 'Charlotte', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="chicago"><?php _ex( 'Chicago', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="cincinnati"><?php _ex( 'Cincinnati', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="cleveland"><?php _ex( 'Cleveland', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="columbia"><?php _ex( 'Columbia', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="dallas"><?php _ex( 'Dallas', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="denver"><?php _ex( 'Denver', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="detroit"><?php _ex( 'Detroit', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="elpaso"><?php _ex( 'El Paso', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="honolulu"><?php _ex( 'Honolulu', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="houston"><?php _ex( 'Houston', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="indianapolis"><?php _ex( 'Indianapolis', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="jackson"><?php _ex( 'Jackson', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="jacksonville"><?php _ex( 'Jacksonville', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="kansas"><?php _ex( 'Kansas City', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="knoxville"><?php _ex( 'Knoxville', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="lasvegas"><?php _ex( 'Las Vegas', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="littlerock"><?php _ex( 'Little Rock', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="losangeles"><?php _ex( 'Los Angeles', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="louisville"><?php _ex( 'Louisville', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="memphis"><?php _ex( 'Memphis', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="miami"><?php _ex( 'Miami', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="milwaukee"><?php _ex( 'Milwaukee', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="minneapolis"><?php _ex( 'Minneapolis', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="mobile"><?php _ex( 'Mobile', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="newheaven"><?php _ex( 'New Heaven', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="neworleans"><?php _ex( 'New Orleans', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="newyork"><?php _ex( 'New York', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="newark"><?php _ex( 'Newark', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="norfolk"><?php _ex( 'Norfolk', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="oklahomacity"><?php _ex( 'Oklahoma City', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="omaha"><?php _ex( 'Omaha', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="philadelphia"><?php _ex( 'Philadelphia', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="phoenix"><?php _ex( 'Phoenix', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="pittsburgh"><?php _ex( 'Pittsburgh', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="portland"><?php _ex( 'Portland', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="richmond"><?php _ex( 'Richmond', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="sacramento"><?php _ex( 'Sacramento', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="saltlakecity"><?php _ex( 'Salt Lake City', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="sanantonio"><?php _ex( 'San Antonio', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="sandiego"><?php _ex( 'San Diego', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="sanfrancisco"><?php _ex( 'San Francisco', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="sanjuan"><?php _ex( 'San Juan', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="seattle"><?php _ex( 'Seattle', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="springfield"><?php _ex( 'Springfield', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="stlouis"><?php _ex( 'St. Louis', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="tampa"><?php _ex( 'Tampa', 'fbi-wanted', 'liquormaster' ); ?></option>
						<option value="washingtondc"><?php _ex( 'Washington DC', 'fbi-wanted', 'liquormaster' ); ?></option>
					</select>
					<div class="notifications">
						<div class="spinner"></div>
						<div class="no-data-response">
							<?php _ex( 'Sorry, there is no data for this office!', 'ajax-playground', 'liquormaster' ); ?>
						</div>
					</div>
				</div>
				<div class="criminals-list-wrapper">
					<ul class="fugitives-list"></ul>
					<div class="pagination-wrapper block-27">
						<ul class="pagination page-numbers"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();
