<?php
/**
 * Theme's 404 template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();

$page_404_id = get_field( '404_page', 'option' );

?>
	<section class="ftco-nothing-found-page">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="page-not-found-content-wrapper">
						<div class="not-found-page-content">
							<?php
							if ( ! empty( $page_404_id ) ) {
								echo get_the_content( '', '', $page_404_id );
							}
							?>
							<div class="search-form-wrapper">
								<?php dynamic_sidebar( 'nothing_found_search' ); ?>
							</div>
						</div>
						<div class="not-found-page-image">
							<?php
							if ( ! empty( $page_404_id ) ) {
								echo get_the_post_thumbnail( $page_404_id );
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();

