<?php
/**
 * Theme's page general template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );



get_header();
?>

	<section class="ftco-section ftco-degree-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 ftco-animate">
					<?php get_template_part( '/template-parts/content/content-page' ); ?>
				</div> <!-- .col-md-8 -->
				<?php if ( yith_wcwl_is_wishlist_page() ) : ?>
					<div class="col-md-4">
						<?php dynamic_sidebar( 'wc_shop_sidebar' ); ?>
					</div>
				<?php else : ?>
					<div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
						<?php dynamic_sidebar( 'content_sidebar' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section> <!-- .section -->


<?php
get_footer();
