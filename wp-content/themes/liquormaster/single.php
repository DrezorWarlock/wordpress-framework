<?php
/**
 * Theme's single general template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>

<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 ftco-animate">
				<?php get_template_part( '/template-parts/content/content-single' ); ?>
			</div> <!-- .col-md-8 -->
			<div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
				<?php dynamic_sidebar( 'content_sidebar' ); ?>
			</div>
		</div>
	</div>
</section> <!-- .section -->


<?php
get_footer();
?>

