//import 'file'

import "popper.js/dist/popper.js";
import "bootstrap/dist/js/bootstrap.js";
import "select2/dist/js/select2.js";
import "jquery.easing/jquery.easing.1.3.js";
import "waypoints/lib/jquery.waypoints.js";
import "jquery.stellar/jquery.stellar.js";
import "owl.carousel/dist/owl.carousel.js";
import "magnific-popup/dist/jquery.magnific-popup.js";
import "jquery.animate-number/jquery.animateNumber.js";
import "scrollax/scrollax.js";
import "chart.js/dist/Chart.js"
import "scripts/general/main.js";
import "scripts/pages/team-members.js";
import "scripts/pages/woocommerce-custom.js";
import "scripts/general/acf-google-map.js";
import "scripts/general/ajax-playground.js";
