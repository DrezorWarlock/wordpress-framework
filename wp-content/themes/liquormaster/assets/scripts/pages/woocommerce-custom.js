/* eslint-disable no-unused-vars */

/* global wc_add_to_cart_params */

$(document).ready(function () {

  function setProductImage(src = null) {
    if (src != null) {
      $('.single-product .images .image-popup').attr('href', src);
      $('.single-product .images .image-popup > img').attr('src', src);
    } else {
      src = $('.single-product .images').data('default-image');
      $('.single-product .images .image-popup').attr('href', src);
      $('.single-product .images .image-popup > img').attr('src', src);
    }
  }

  $('form.cart').on('submit', function (e) {
    e.preventDefault();

    let form = $(this);

    let variableProduct = (form.find('.variation_id').length > 0);

    let formData = form.serializeArray();
    if (variableProduct) {
      formData.filter((item, index) => {
          switch (item.name) {
            case 'add-to-cart':
              delete formData[index];
              break;

            case 'product_id':
              item.value = form.find('.variation_id').val();
              break;

            default:
          }
        },
      )
    } else {
      let productID = form.find('.single_add_to_cart_button').val();

      formData.push({name: 'product_id', value: productID});
    }

    $.ajax({
      type: 'POST',
      url: wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'),
      data: formData,
      dataType: 'json',
      beforeSend: () => {
        $('.spinner').css({'opacity': 1})
      },
      success: function (response) {
        $('.spinner').css({'opacity': 0})
        if (!response) {
          return;
        }

        if (response.error && response.product_url) {
          window.location = response.product_url;
          return;
        }

        // Trigger event so themes can refresh other areas.
        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, form]);
      },
    });
  });

  $('.quantity-right-plus').click(function (e) {

    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    var quantity = parseInt($('#quantity').val());

    // If is not undefined

    $('#quantity').val(quantity + 1);


    // Increment

  });

  $('.quantity-left-minus').click(function (e) {
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    var quantity = parseInt($('#quantity').val());

    // If is not undefined

    // Increment
    if (quantity > 0) {
      $('#quantity').val(quantity - 1);
    }
  });

  $('#quantity').on('change', function () {
    if ($(this).val() <= 0) {
      $(this).val(1);
    }
  });

  $(document).on('added_to_cart', function (e, fragments, hash, button) {
    $(button).siblings('.added_to_cart').show();

    setTimeout(() => {
      $(button).removeClass('added');
      $(button).siblings('.added_to_cart').hide();
    }, 5000);

  })

  $('.single-product .images .product-gallery-thumbnails .thumbnail img').on('click', function () {
    let imgSrc = $(this).data('large-src');
    setProductImage(imgSrc);
  })


  $(document).on('show_variation', function (event, variation) {
    if (variation.image.url.length > 0) {
      setProductImage(variation.image.url);
    } else {
      setProductImage();
    }
  });

  $(document).on('reset_data', function () {
    setProductImage();
  })

  $('.quantity-left-minus').on('click', function () {
    let currentQuantity = parseInt($('#quantity').val());

    if (currentQuantity < 1) {
      $('#quantity').val(1);
    }
  });

  let wishlistCount = localStorage.getItem('total_items_wishlist');

  if (wishlistCount > 0) {
    $('#total-items-wishlist small').text(wishlistCount);
  } else {
    $('#total-items-wishlist small').text(0);
  }

  $(document).on('added_to_wishlist removed_from_wishlist', function (event, target_button) {
    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_wishlist_count',
      beforeSend: () => {
        $('.spinner').show();
      },
      success: (response) => {
        localStorage.setItem('total_items_wishlist', response.wishlist_count);
        $('.btn-wishlist').attr('href', response.wishlist_url);
        $('#total-items-wishlist small').text(response.wishlist_count);
        $(target_button).find('.fa-heart-o').removeClass('fa-heart-o').addClass('fa-heart');
        $(target_button).removeClass('add_to_wishlist single_add_to_wishlist');
        $(target_button).attr('href', '#');
        $('.spinner').hide();
      },
    });
  })

  let compareCount = localStorage.getItem('total_items_compare');

  if (compareCount > 0) {
    $('#total-items-compare small').text(compareCount);
  } else {
    $('#total-items-compare small').text(0);
  }

  $(document).on('yith_woocompare_open_popup yith_woocompare_product_removed', function (e) {
    console.log(e.type);
    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_compare_count',
      success: (response) => {
        if (e.type == 'yith_woocompare_product_removed') {
          $(window.parent.document).find('#total-items-compare').find('small').text(response.compare_count);
        } else {
          $('#total-items-compare small').text(response.compare_count);
        }
        localStorage.setItem('total_items_compare', response.compare_count);
      },
    });
  });

  $(document).on('berocket_ajax_products_loaded', function () {
    $('.berocket_single_filter_widget select.selectpicker').selectpicker('refresh');

    $('html, body').animate({
      scrollTop: $('.shop-block').offset().top,
    }, 1000);
  });

  $(document).on('blur', '.product-quantity .qty', function () {
    $('[name="update_cart"]').trigger('click');
  })

  $(document).on('click', '.product-checkbox input', function () {
    let checkedBoxes = $('.product-checkbox input:checked').length;

    if (checkedBoxes > 0) {
      $('.product-mass-delete span').show();
    } else {
      $('.product-mass-delete span').hide();
    }
  });

  $(document).on('click', '.product-mass-delete span', function () {
    let checkedBoxes = $('.product-checkbox input:checked');


    let productIDs = [];

    for (let checkedBox of checkedBoxes) {
      productIDs.push($(checkedBox).data('product-id'));
    }

    $.ajax({
      url: themeVars.ajaxurl,
      method: 'POST',
      data: 'action=remove_checked_products_from_cart&products_list=' + productIDs,
      beforeSend: () => {
        $(this).siblings('.spinner').css({'opacity': 1, 'display': 'block'});
      },
      success: (response) => {
        $(this).siblings('.spinner').css({'opacity': 0, 'display': 'none'});
        if(response.hasOwnProperty('success')) {
          $(document).trigger('wc_update_cart');
        }
      },
    });
  });

  $('#calc_shipping_state').on('change', function() {
    let currentState = $(this).val();

    if(currentState === 'Poltava') {
      $('#calc_shipping_city').val(currentState);
    } else {
      $('#calc_shipping_city').val('');
    }
  });

  let currentBillingState = $('#billing_city').val();

  if(currentBillingState === 'Poltava') {
    $('#billing_city').val(currentBillingState).attr('readOnly', true).addClass('home-state');
  } else {
    $('#billing_city').val('').attr('readOnly', false).removeClass('home-state');
  }

  let currentShippingState = $('#shipping_city').val();

  if(currentShippingState === 'Poltava') {
    $('#shipping_city').val(currentShippingState).attr('readOnly', true).addClass('home-state');
  } else {
    $('#shipping_city').val('').attr('readOnly', false).removeClass('home-state');
  }

  $('#billing_state').on('change', function() {
    let currentState = $(this).val();

    if(currentState === 'Poltava') {
      $('#billing_city').val(currentState).attr('readOnly', true).addClass('home-state');
    } else {
      $('#billing_city').val('').attr('readOnly', false).removeClass('home-state');
    }

    $('#billing_address_1, #billing_address_2, #billing_postcode').val('');
  });

  $('#shipping_state').on('change', function() {
    let currentState = $(this).val();

    if(currentState === 'Poltava') {
      $('#shipping_city').val(currentState).attr('readOnly', true).addClass('home-state');
    } else {
      $('#shipping_city').val('').attr('readOnly', false).removeClass('home-state');
    }

    $('#shipping_address_1, #shipping_address_2, #shipping_postcode').val('');
  });

});

