/* eslint-disable no-unused-vars */

$(document).ready(function () {
  $('.departments-list .department').on('click', function () {
    $('.departments-list .department').removeClass('active');
    $(this).addClass('active');

    let departmentId = $(this).data('department');

    $('.team-member').removeClass('active');
    $('.team-member').removeClass('fadeInUp');
    $('.team-member').removeClass('ftco-animate');
    $('.team-member').removeClass('ftco-animated');

    if (departmentId == '0') {
      $('.team-member').addClass('active');
    } else {
      $('.team-member[data-department="' + departmentId + '"]').addClass('active');
    }
  })
});
