/* eslint-disable no-unused-vars */
/* global Chart*/

$(document).ready(function () {
  let countriesList = $('#covid-countries');
  let fbiFieldOffices = $('#fbi-field-offices');
  let privatExchangeResult = $('#privat-exchange .privat-bank-request-result');
  let covidInfo = $('#covid .covid-request-result .covid-info');
  let fugitivesList = $('.fugitives-list');

  /**
   * Function to get Privat Bank Exchange Rate Archive Data
   */
  function get_privat_bank_archive_exchange_data() {
    let date = $('#exchange-archive-date').val();

    if (date === undefined || date.length < 1) {
      $('#privat-archive .request-controls .notification').show();
    } else {
      $('#privat-archive .request-controls .notifications .notification').hide();
      $('.spinner').css({'opacity': 1});
      $.ajax({
        url: themeVars.ajaxurl,
        method: 'GET',
        data: 'action=get_privat_bank_archive_currency_exchange&date=' + date,
        success: (response) => {
          $('.spinner').css({'opacity': 0});
          $('#privat-archive .privat-bank-request-result').show();
          $('#privat-archive .privat-bank-request-result .ce-info').empty();

          let responseHtml = null;
          let baseCurrency = null;
          let currency = null;
          let purchaseRateNB = null;
          let saleRateNB = null;
          let purchaseRate = null;
          let saleRate = null;
          for (let ceInfo of response.exchangeRate) {
            baseCurrency = ceInfo.baseCurrency;
            currency = (ceInfo.currency === undefined) ? ceInfo.baseCurrency : ceInfo.currency;
            purchaseRateNB = ceInfo.purchaseRateNB;
            saleRateNB = ceInfo.saleRateNB;
            purchaseRate = (ceInfo.purchaseRate === undefined) ? '' : ceInfo.purchaseRate;
            saleRate = (ceInfo.saleRate === undefined) ? '' : ceInfo.saleRate;

            responseHtml = `
              <tr>
                <td>${baseCurrency}</td>
                <td>${currency}</td>
                <td>${purchaseRateNB}</td>
                <td>${saleRateNB}</td>
                <td>${purchaseRate}</td>
                <td>${saleRate}</td>
              </tr>
            `
            $('#privat-archive table .ce-info').append(responseHtml)
          }
        },
      })
    }
  }

  /**
   * Function to get Privat Bank Exchange Rate
   */
  function get_privat_bank_exchange_data() {
    $('.spinner').css({'opacity': 1});
    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_privat_bank_currency_exchange_data',
      success: (response) => {
        $('.spinner').css({'opacity': 0});
        privatExchangeResult.show();
        privatExchangeResult.find('.ce-info').empty();

        let responseHtml = null;
        let baseCurrency = null;
        let currency = null;
        let purchaseRate = null;
        let saleRate = null;
        for (let ceInfo of response) {
          baseCurrency = ceInfo.base_ccy;
          currency = ceInfo.ccy;
          purchaseRate = ceInfo.buy;
          saleRate = ceInfo.sale;

          responseHtml = `
              <tr>
                <td>${baseCurrency}</td>
                <td>${currency}</td>
                <td>${purchaseRate}</td>
                <td>${saleRate}</td>
              </tr>
            `
          privatExchangeResult.find('.ce-info').append(responseHtml)
        }
      },
    })
  }

  /**
   * Function to get list of countries reported about COVID-19 spreading
   */
  function get_covid_countries_data() {
    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_covid_countries_data',
      beforeSend: () => {
        $('#covid .request-controls .notifications .no-data-response').hide();
        $('.spinner').css({'opacity': 1});
      },
      success: (response) => {
        if (response.code == 404) {

          $('.spinner').css({'opacity': 0});
          $('.no-data-response').text(response.message);
          $('#covid .request-controls .notifications .no-data-response').show();

          return;
        }

        $('.spinner').css({'opacity': 0});
        $('#covid .request-button').hide();

        countriesList.append(response);

        $('#covid-countries + .select2-container').show();
      },

    });
  }

  /**
   * Function to get COVID-19 spreading data for specific country. Used to build data table.
   * @param selectValue country's ISO2 code
   */
  function get_covid_country_data(selectValue) {
    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_covid_country_data&country=' + selectValue,
      beforeSend: () => {
        $('.no-data-response').hide();
        $('.spinner').css({'opacity': 1});
      },
      success: (response) => {

        if (response.code == 404) {

          $('.spinner').css({'opacity': 0});
          $('.no-data-response').text(response.message);
          $('#covid .request-controls .notifications .no-data-response').show();

          return;
        }
        $('.spinner').css({'opacity': 0});

        if (response.length == 0) {
          $('#covid .request-controls .notifications .no-data-response').show();
          $('#covid .covid-request-result, .chart-block').hide();
        } else {
          covidInfo.empty();
          covidInfo.append(response.table_data);
          $('#covid .covid-request-result, .chart-block').show();
          build_covid_chart_table(response.chart_data);
        }
      },
    });
  }

  /**
   * Function to build chart based on COVID-19 spreading data for specific country
   * @param chartData
   */
  function build_covid_chart_table(chartData) {
    let countryChart;

    let ctx = document.getElementById('country-chart').getContext('2d');
    let data = {
      labels: chartData.labels,
      datasets: [
        {
          label: chartData.data_labels.active,
          data: chartData.data.active,
          backgroundColor: 'rgba(255,165,0,0.2)',
          borderColor: 'rgba(255,165,0,1)',
          fill: false,
          borderWidth: 1,
        },
        {
          label: chartData.data_labels.сonfirmed,
          data: chartData.data.confirmed,
          backgroundColor: 'rgba(255,69,0,0.2)',
          borderColor: 'rgba(255,69,0,1)',
          fill: false,
          borderWidth: 1,
        },
        {
          label: chartData.data_labels.recovered,
          data: chartData.data.recovered,
          backgroundColor: 'rgba(202,255,112,0.2)',
          borderColor: 'rgba(202,255,112,1)',
          fill: false,
          borderWidth: 1,
        },
        {
          label: chartData.data_labels.deaths,
          data: chartData.data.deaths,
          backgroundColor: 'rgba(0,0,0,0.2)',
          borderColor: 'rgba(0,0,0,1)',
          fill: false,
          borderWidth: 1,
        },
      ],
    }
    if (typeof countryChart === 'undefined') {
      countryChart = new Chart(ctx, {
        type: 'line',
        data: data,
      });
    } else {
      countryChart.data = data;

      countryChart.update();
    }
  }

  /**
   * Function to get fugitives data for specific field office
   * @param select field office name
   */
  function get_fbi_wanted_data(select) {
    if ($(select).val() == 'false') {
      return;
    }

    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_fbi_wanted_list&field_office=' + $(select).val(),
      beforeSend: () => {
        $('.no-data-response').hide();
        $('.spinner').css({'opacity': 1});
      },
      success: (response) => {
        $('.fugitives-list, .pagination-wrapper .pagination').empty();

        if (response.items.length > 0) {
          fugitivesList.append(response.items);
          $('.pagination-wrapper .pagination').append(response.pages);
        } else {
          $('.no-data-response').show();
        }
        $('.spinner').css({'opacity': 0});
      },
    });
  }

  countriesList.select2();

  fbiFieldOffices.select2();

  $('#privat-archive .request-button').on('click', function () {
    get_privat_bank_archive_exchange_data();
  });

  $('#privat-exchange .request-button').on('click', function () {
    get_privat_bank_exchange_data();
  });

  $('#covid .request-button').on('click', function () {
    get_covid_countries_data();
  });

  countriesList.on('change', function () {
    let country = $(this).val();
    get_covid_country_data(country);
  });

  fbiFieldOffices.on('change', function () {
    get_fbi_wanted_data(this);
  })

  $('.pagination.page-numbers').on('click', 'li', function () {
    let currentItem = $(this)

    if (currentItem.hasClass('current')) {
      return;
    }

    $.ajax({
      url: themeVars.ajaxurl,
      method: 'GET',
      data: 'action=get_fbi_wanted_list&field_office=' + fbiFieldOffices.val() + '&page=' + $(this).data('page'),
      beforeSend: () => {
        $('.no-data-response').hide();
        $('.spinner').css({'opacity': 1});
      },
      success: (response) => {
        fugitivesList.empty();
        fugitivesList.append(response.items);
        $('.spinner').css({'opacity': 0});
        $('.pagination.page-numbers > li').removeClass('current');
        currentItem.addClass('current');
        $('html, body').animate({
          scrollTop: $('.field-offices-list-wrapper').offset().top,
        }, 1000);
      },
    });
  })
});
