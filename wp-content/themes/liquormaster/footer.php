<?php
/**
 * Theme's main footer template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$newsletter_form_id = get_field( 'newsletter_form_id', 'option' );

?>


<footer class="ftco-footer">
	<div class="container">
		<div class="row mb-5">
			<div class="col-sm-12 col-md">
				<?php dynamic_sidebar( 'footer_left' ); ?>
			</div>
			<div class="col-sm-12 col-md">
				<div class="ftco-footer-widget mb-4 ml-md-4">
					<?php dynamic_sidebar( 'footer_left_middle' ); ?>
				</div>
			</div>
			<div class="col-sm-12 col-md">
				<div class="ftco-footer-widget mb-4 ml-md-4">
					<?php dynamic_sidebar( 'footer_middle' ); ?>
				</div>
			</div>
			<div class="col-sm-12 col-md">
				<div class="ftco-footer-widget mb-4">
					<?php dynamic_sidebar( 'footer_right_middle' ); ?>
				</div>
			</div>
			<div class="col-sm-12 col-md">
				<div class="ftco-footer-widget mb-4">
					<?php dynamic_sidebar( 'footer_right' ); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid px-0 py-5 bg-black">
		<div class="container">
			<div class="row">
				<div class="col-md-7	">

					<p class="mb-0" style="color: rgba(255,255,255,.5);">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						<?php _e( 'Copyright', 'liquormaster' ) ?> &copy; <?php echo date( 'Y' ) ?>
						<?php _e( 'All rights reserved', 'liquormaster' ) ?> | <?php echo sprintf( __('This template is made with %s by', 'liquormaster'), '<i
								class="fa fa-heart color-danger"
								aria-hidden="true"></i>' ) ?> <a
								href="https://colorlib.com" target="_blank">Colorlib.com</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
				<div class="col-md-5">
					<?php echo do_shortcode( '[contact-form-7 id="' . $newsletter_form_id . '" html_class="news-letter-form"]' ) ?>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
	<svg class="circular" width="48px" height="48px">
		<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
		<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
				stroke="#F96D00"/>
	</svg>
</div>
<?php wp_footer(); ?>
</body>
</html>
