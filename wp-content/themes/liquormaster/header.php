<?php
/**
 * Theme's main header template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

require_once 'inc/template-classes/class-liqmthemeheader.php';

$header = new LiqmThemeHeader();
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php
echo $header->get_header_wrap();
echo $header->get_header_nav();
echo $header->get_header_hero();
?>
