<?php
/**
 * Main template of WooCommerce plugin.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

if ( is_shop() || is_product_category() ) {
	shop_dropdown();
}

get_header();

if ( is_product() ) {
	wc_get_template_part( 'template-parts/woocommerce/wc', 'product' );
} else {
	wc_get_template_part( 'template-parts/woocommerce/wc', 'shop' );
}

get_footer();
