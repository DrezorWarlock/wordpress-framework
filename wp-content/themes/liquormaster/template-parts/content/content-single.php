<?php
/**
 * Single page content template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$post_thumbnail_url        = get_the_post_thumbnail_url();
$post_author_id            = get_the_author_meta( 'ID' );
$post_thumbnail            = ( ! empty( $post_thumbnail_url ) ) ? "<img src='" . $post_thumbnail_url . "'/>" : null;
$post_author_custom_avatar = get_the_author_meta( 'liq_user_avatar' );

echo $post_thumbnail;

the_content();
?>

<div class="tag-widget post-tag-container mb-5 mt-5">
	<div class="tagcloud">
		<?php
		wp_tag_cloud(
			array(
				'include' => wp_get_post_tags( get_the_ID(), array( 'fields' => 'ids' ) ),
			)
		)
		?>
	</div>
</div>

<div class="about-author d-flex p-4 bg-light">
	<div class="bio mr-5">
		<?php if ( ! empty( wp_get_attachment_image_url( $post_author_custom_avatar, 'full' ) ) ): ?>
			<img src="<?php echo wp_get_attachment_image_url( $post_author_custom_avatar, 'full' ); ?>"
				 alt="<?php _e( 'Author Photo', 'liquormaster' ); ?>" class="img-fluid mb-4"/>
		<?php else: ?>
			<img src="<?php echo get_field( 'default_user_image', 'option' ); ?>"
				 alt="<?php _e( 'Author Photo', 'liquormaster' ); ?>" class="img-fluid mb-4"/>
		<?php endif; ?>
	</div>
	<div class="desc">
		<h3><?php echo get_the_author_meta( 'nickname' ); ?></h3>
		<p><?php echo get_the_author_meta( 'user_description' ); ?></p>
	</div>
</div>


<div class="pt-5 mt-5">

	<?php
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>
</div>
