<?php
/**
 * Single page content template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$post_thumbnail_url = get_the_post_thumbnail_url();

$post_thumbnail = ( ! empty( $post_thumbnail_url ) ) ? "<img src='" . $post_thumbnail_url . "'/>" : null;

echo $post_thumbnail;

the_content();

