<?php
/**
 * Single page content template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$post_thumbnail_url = get_the_post_thumbnail_url();

$post_thumbnail      = ( ! empty( $post_thumbnail_url ) ) ? "<img src='" . $post_thumbnail_url . "'/>" : null;
$department_name     = get_term( get_field( 'team_member_department' ) )->name;
$department_position = get_field( 'team_member_position' );
$location            = get_term( get_field( 'team_member_location' ) )->name;
?>

<div class="team-member-info-block-wrapper">
	<div class="team-member-image">
		<?php echo $post_thumbnail ?>
	</div>
	<div class="team-member-info">
		<div class="team-member-department team-member-info-block">
			<span class="team-member-info-block-label">
				<?php echo __( "Department: ", 'liquormaster' ); ?>
			</span>
			<?php echo $department_name ?>
		</div>
		<span class="separator">,</span>
		<div class="team-member-position team-member-info-block">
			<span class="team-member-info-block-label">
				<?php echo __( "Position: ", 'liquormaster' ); ?>
			</span>
			<?php echo $department_position ?>
		</div>
		<div class="team-member-location team-member-info-block">
			<span class="team-member-info-block-label">
				<?php echo __( 'From:', 'liquormaster' ); ?>
			</span>
			<?php echo $location ?>
		</div>
	</div>
</div>
<div class="team-member-content">
	<?php the_content(); ?>
</div>
