<?php

/**
 * Template part for main shop page
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

?>

<section class="ftco-section shop-block">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="row mb-4">
					<?php if ( is_shop() || is_product_category() ): ?>
						<div class="col-md-12 shop-filters-block d-flex justify-content-center align-items-center">
							<?php echo do_shortcode('[br_filters_group group_id=1781]'); ?>
						</div>
					<?php endif; ?>
				</div>
				<?php
				woocommerce_content();
				?>
			</div>
			<div class="col-md-3">
				<?php dynamic_sidebar( 'wc_shop_sidebar' ); ?>
			</div>
		</div>
	</div>
</section>
