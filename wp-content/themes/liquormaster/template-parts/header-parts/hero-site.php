<?php
/**
 * Site pages hero section
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

?>
<section class="hero-wrap hero-wrap-2"
		 style="background-image: url(<?php the_field( 'hero_section_background_image', 'options' ) ?>);"
		 data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate mb-5 text-center">
				<?php
				if ( ! is_404() ) {
					yoast_breadcrumb( '<p class="breadcrumbs mb-0">', '</p>' );
				}
				?>
				<?php if ( ! empty( $args['page_title'] ) ) : ?>
					<h2 class="mb-0 bread"><?php echo $args['page_title']; ?></h2>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
