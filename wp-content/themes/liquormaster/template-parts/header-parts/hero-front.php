<?php
/**
 * Front page hero section
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );
$shop_now_button = get_field( 'front_page_shop_now_button', 'option' );
$read_more_button = get_field( 'front_page_read_more_button', 'option' );
?>
<div class="hero-wrap" style="background-image: url('<?php echo THEME_DIR_URI ?>/assets/images/bg_2.jpg');"
	 data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-center justify-content-center">
			<div class="col-md-8 ftco-animate d-flex align-items-end">
				<div class="text w-100 text-center">
					<h1 class="mb-4"><?php the_field( 'front_page_banner_text', 'option' ); ?></h1>
					<p>
						<?php if ( ! empty( $shop_now_button ) ): ?>
							<a href="#"
							   class="btn btn-primary py-2 px-4"><?php the_field( 'front_page_shop_now_button', 'option' ); ?></a>
						<?php endif; ?>
						<?php if ( ! empty( $read_more_button ) ): ?>
							<a href="#"
							   class="btn btn-white btn-outline-white py-2 px-4"><?php the_field( 'front_page_read_more_button', 'option' ); ?></a>
						<?php endif; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
