<?php
/**
 * Numerals helper function
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

/**
 * Function to process numerals. Meant to be used for languages having 3 or more numerals forms.
 *
 * @param integer $number digit for numerals.
 * @param array   $titles text for numerals.
 *
 * @return string
 */
function declOfNum( $number, $titles ) {
	$cases  = array( 2, 0, 1, 1, 1, 2 );
	$format = $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];

	return sprintf( $format, $number );
}
