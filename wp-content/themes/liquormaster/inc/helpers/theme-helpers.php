<?php
/**
 * Theme-specific helpers
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

function init_acf_google_map() {
	add_action( 'wp_enqueue_scripts', function () {
		wp_enqueue_script( 'google-maps-api', get_template_directory_uri() . '/assets/scripts/general/google-map.js', [], null, true );
	} );

	add_action( 'wp_enqueue_scripts', function () {
		wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?language=' . ICL_LANGUAGE_CODE . '&key=' . get_field( 'google_map_api_key', 'option' ), [], null, true );
	} );
}


function shop_dropdown() {
	add_action( 'wp_enqueue_scripts', function () {
		wp_enqueue_style( 'shop-dropdown-css', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css', [], '1.13.1' );
	} );

	add_action( 'wp_enqueue_scripts', function () {
		wp_enqueue_script( 'shop-dropdown-js', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js', [ 'jquery' ], '1.13.1', true );
	} );
}
