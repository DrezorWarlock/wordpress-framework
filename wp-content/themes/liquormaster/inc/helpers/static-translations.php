<?php
/**
 * Collection of strings that are meant to be used throughout the website
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );


_x( 'January', 'months list', 'liquormaster' );
_x( 'February', 'months list', 'liquormaster' );
_x( 'March', 'months list', 'liquormaster' );
_x( 'April', 'months list', 'liquormaster' );
_x( 'May', 'months list', 'liquormaster' );
_x( 'June', 'months list', 'liquormaster' );
_x( 'July', 'months list', 'liquormaster' );
_x( 'August', 'months list', 'liquormaster' );
_x( 'September', 'months list', 'liquormaster' );
_x( 'October', 'months list', 'liquormaster' );
_x( 'November', 'months list', 'liquormaster' );
_x( 'December', 'months list', 'liquormaster' );
