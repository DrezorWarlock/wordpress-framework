<?php

/**
 * Class Liqm_Footer_Nav_Menu_Walker
 *
 * @package WordPress
 */
class Liqm_Comments_Walker extends Walker_Comment {

	/**
	 * Method to render opening tag for element list item.
	 *
	 * @param string     $output HTML output of the element.
	 * @param WP_Comment $comment object of current comment.
	 * @param int        $depth depth of current comment.
	 * @param array      $args arguments for current list.
	 * @param int        $id id of current comments list.
	 *
	 * @return string
	 */
	public function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ): string {


		$author_name = $comment->comment_author;

		if ( email_exists( $comment->comment_author_email ) ) {
			$author_id        = get_user_by( 'email', $comment->comment_author_email )->data->ID;
			$author_avatar_id = get_the_author_meta( 'liq_user_avatar', $author_id );
		}

		$author_avatar_url = ( ! empty( $author_avatar_id ) ) ? wp_get_attachment_image_url( $author_avatar_id ) :
			get_field( 'default_user_image', 'option' );
		$comment_date      = get_comment_date( get_field( 'comments_date_format', 'option' ), $comment->comment_ID );
//		$comment_reply_button = _x('Reply', 'comment texts', 'liquormaster');
		$comment_reply_button = get_comment_reply_link(
			array(
				'depth'     => 1,
				'max_depth' => $args['max_depth'],
			), $comment->comment_ID, $comment->comment_post_ID );
		$output               .= <<<HTML
<li class="comment" id="comment-{$comment->comment_ID}">
	<div id="div-comment-{$comment->comment_ID}">
		<div class="vcard bio">
			<img src="{$author_avatar_url}" alt="Image placeholder">
		</div>
		<div class="comment-body">
			<h3>{$author_name}</h3>
			<div class="meta">{$comment_date}</div>
			<p>{$comment->comment_content}</p>
			<div class="reply">{$comment_reply_button}</div>
		</div>
	</div>
</li>
HTML;

		return $output;
	}

	/**
	 * Method to render closing tag for element list item.
	 *
	 * @param string     $output HTML output of the element.
	 * @param WP_Comment $comment object of current comment.
	 * @param int        $depth depth of current comment.
	 * @param array      $args arguments for current list.
	 * @param int        $id identifier of current comments list.
	 *
	 * @return string
	 */
	public function end_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ): string {
		$output .= '</li>';

		return $output;
	}
}
