<?php
/**
 * Class Liqm_Footer_Nav_Menu_Walker
 *
 * @package WordPress
 */
class Liqm_Footer_Nav_Menu_Walker extends Walker_Nav_Menu {
	/**
	 * Function to configure opening elements of navigation menu.
	 *
	 * @param string     $output html output of current menu item.
	 * @param WP_Post    $item object of current menu item.
	 * @param int        $depth depth of current menu item.
	 * @param array|null $args arguments of navigation menu passed via wp_nav_menu() function.
	 * @param int        $id current menu item id.
	 *
	 * @return string
	 */
	public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ): string {
		$output .= '<li>';
		$output .= '<a href="' . $item->url . '"><span class="fa ' . get_field( 'link_item_icon', 'option' ) . ' mr-2"></span>' . $item->post_title . '</a>';

		return $output;
	}

	/**
	 * Function to configure closing elements of navigation menu.
	 *
	 * @param string     $output html output of current menu item.
	 * @param WP_Post    $item object of current menu item.
	 * @param int        $depth depth of current menu item.
	 * @param array|null $args arguments of navigation menu passed via wp_nav_menu() function.
	 * @param int        $id current menu item id.
	 *
	 * @return string
	 */
	public function end_el( &$output, $item, $depth = 0, $args = null, $id = 0 ): string {
		$output .= '</li>';
		return $output;
	}
}
