<?php

require_once 'class-liqmwebsiteblocks.php';

/**
 * Class for about us page render.
 *
 * @package WordPress
 */
class LiqmAboutUs extends LiqmWebsiteBlocks {

	/**
	 * Property to store post's custom fields values.
	 *
	 * @var mixed
	 */
	public $acf_post_fields;

	/**
	 * LiqmAboutUs constructor.
	 */
	public function __construct() {
		$this->acf_post_fields = get_field( 'about_us_info_blocks_website_blocks', get_the_ID() );
		parent::__construct();
	}

	/**
	 * Method to render list of blocks.
	 *
	 * @return bool
	 */
	public function render() {
		if ( ! empty( $this->acf_post_fields ) ) {
			foreach ( $this->acf_post_fields as $acf_post_field ) {
				echo call_user_func(
					array(
						$this,
						'get_' . $acf_post_field['acf_fc_layout'],
					),
					$acf_post_field['layout_data']
				);
			}

			return true;
		}

		return false;
	}

	/**
	 * Method to render informational block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_informational_blocks( $data ) {
		if ( ! empty( $data ) ) :
			$data_html = null;

			foreach ( $data as $index => $data_item ) :
				$data_html .= <<<HTML
<div class="col-md-4 d-flex">
    <div class="intro color-{$index} d-lg-flex w-100 ftco-animate">
        <div class="icon">
            <span class="{$data_item['intro_subblock_icon']}"></span>
        </div>
        <div class="text">
            <h2>{$data_item['intro_subblock_header']}</h2>
            <p>{$data_item['intro_subblock_text']}</p>
        </div>
    </div>
</div>
HTML;
			endforeach;

			$block = <<<HTML
    <section class="ftco-intro">
    	<div class="container">
    		<div class="row no-gutters">
				{$data_html}
    		</div>
    	</div>
    </section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render main info block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_main_info( $data ) {
		if ( ! empty( $data ) ) :
			$left_image = $right_image = null;

			if ( $data['main_info_layout_type'] == 'image_left' ) :
				$left_image = <<<HTML
<div class="col-md-6 img img-3 d-flex justify-content-center align-items-center" style="background-image: url({$data['main_info_image']});">
					</div>
HTML;
			endif;

			if ( $data['main_info_layout_type'] == 'image_right' ) :
				$right_image = <<<HTML
<div class="col-md-6 img img-3 d-flex justify-content-center align-items-center" style="background-image: url({$data['main_info_image']});">
					</div>
HTML;
			endif;

			$block = <<<HTML
<section class="ftco-section ftco-no-pb">
	<div class="container">
		<div class="row">
			{$left_image}
			<div class="col-md-6 wrap-about pl-md-5 ftco-animate py-5">
				<div class="heading-section">
        			<span class="subheading">{$data['main_info_subheading']}</span>
                    {$data['main_info_text']}
			        <p class="year">
			            <strong class="number" data-number="{$data['main_info_years_of_experience']['experience_years_number']}">0</strong>
			            <span>{$data['main_info_years_of_experience']['experience_text']}</span>
			        </p>
      			</div>
			</div>
			{$right_image}
		</div>
	</div>
</section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render alcohol types block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_alcohol_types( $data ) {
		if ( ! empty( $data ) ) :

			$block_content = null;
			foreach ( $data as $data_items ) :
				foreach ( $data_items as $data_item ) :
					$alcohol_type_image = get_term_meta( $data_item, 'thumbnail_id', true );
					$block_image        = wp_get_attachment_image_url( $alcohol_type_image );

					$alcohol_type_name = get_term( $data_item, 'product_cat' )->name;
					$alcohol_type_link = get_term_link( $data_item );

					$block_content .= <<<HTML
<div class="col-lg-2 col-md-4 ">
	<div class="sort w-100 text-center ftco-animate">
		<a href="{$alcohol_type_link}">
			<div class="img" style="background-image: url({$block_image});"></div>
			<h3>{$alcohol_type_name}</h3>
		</a>
	</div>
</div>
HTML;
				endforeach;
			endforeach;

			$block = <<<HTML
<section class="ftco-section">
			<div class="container">
				<div class="row">
					{$block_content}
				</div>
			</div>
		</section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_testimonials( $data ) {
		if ( ! empty( $data ) ) :
			$block_subheading        = __( 'Testimonial', 'liquormaster' );
			$testimonials_list_items = null;
			$testimonials_item_icon = get_field('testimonials_item_icon', 'option');

			foreach ( $data['testimonials_list'] as $testimonial ) :
				$testimonials_list_items .= <<<HTML
<div class="item">
	<div class="testimony-wrap py-4">
		<div class="icon d-flex align-items-center justify-content-center"><span class="fa {$testimonials_item_icon}"></div>
			<div class="text">
				<p class="mb-4">{$testimonial['testimonial_text']}</p>
				<div class="d-flex align-items-center">
					<div class="user-img" style="background-image: url({$testimonial['testimonial_author_image']})">
				</div>
				<div class="pl-3">
					<p class="name">{$testimonial['testimonial_author_name']}</p>
					<span class="position">{$testimonial['testimonial_author_position']}</span>
				</div>
			</div>
		</div>
	</div>
</div>
HTML;
			endforeach;
			$block = <<<HTML
<section class="ftco-section testimony-section img" style="background-image: url({$data['testimonials_background_image']});">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
          	<span class="subheading">{$block_subheading}</span>
            <h2 class="mb-3">{$data['testimonials_header_text']}</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              {$testimonials_list_items}
            </div>
          </div>
        </div>
      </div>
    </section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_counters_list( $data ) {
		if ( ! empty( $data ) ) :
			$counters_list = null;
			foreach ( $data['counters'] as $counter ) :
				$counters_list .= <<<HTML
<div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
	<div class="block-18 py-4 mb-4">
	  <div class="text align-items-center">
	    <strong class="number" data-number="{$counter['counter_number']}">0</strong>
	    <span>{$counter['counter_text']}</span>
	  </div>
	</div>
</div>
HTML;
			endforeach;

			$block = <<<HTML
<section class="ftco-counter ftco-section ftco-no-pt ftco-no-pb img bg-light" id="section-counter">
	<div class="container">
	    <div class="row">
			{$counters_list}
	    </div>
	</div>
</section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render offers on front page.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string|void
	 */
	protected function get_offers( $data ) {
	}

	/**
	 * Method to render recent blog according to post type blog settings.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string|null|void
	 */
	protected function get_recent_blog( $data ) {
	}
}
