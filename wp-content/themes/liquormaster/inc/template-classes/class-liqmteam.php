<?php

/**
 * Class for team archive page render.
 *
 * @package WordPress
 */
class LiqmTeam {

	/**
	 * Property to store list of departments.
	 *
	 * @var int[]|string|string[]|WP_Error|WP_Term[]
	 */
	private $departments;

	/**
	 * Property to store template directory path.
	 *
	 * @var string
	 */
	private $template_directory;

	/**
	 * LiqmTeam constructor.
	 */
	public function __construct() {
		$this->template_directory = THEME_DIR_URI;
		$this->departments        = get_terms(
			array(
				'taxonomy'     => 'product_cat',
				'hide_empty'   => false,
				'exclude_tree' => 29,
			)
		);
	}

	/**
	 * Method to render team's archive page content.
	 *
	 * @return null
	 */
	public function render() {
		$block = <<<HTML
<section class="ftco-section team-list-section">
	<div class="container">
HTML;
		if ( have_posts() ) :
			$block .= <<<HTML
		<div class="row d-flex">
			<div class="col-lg-12 d-flex align-items-stretch ftco-animate team-list-wrapper">
				{$this->get_departments_list()}
			</div>
		</div>
		<div class="row d-flex">
			{$this->get_all_employees()}
		</div>
HTML;
		else :
			$nothing_found_text = _x( 'Team members coming soon', 'archive team no posts found', 'liquormaster' );
			$block              .= <<<HTML
<div class="row d-flex">
	<div class="col-lg-12 d-flex justify-content-center align-items-stretch ftco-animate">
		<h3>
			{$nothing_found_text}
		</h3>
	</div>
</div>
HTML;
		endif;
		$block .= <<<HTML
	</div>
</section>
HTML;

		echo $block;

		return null;
	}

	/**
	 * Method to get department names list.
	 *
	 * @return string
	 */
	private function get_departments_list(): string {
		if ( ! empty( $this->departments ) ) :
			$block = '<ul class="departments-list">';
			$block .= '<li class="department active" data-department="0">' . _x( 'All', 'team-members list', 'liquormaster' ) . '</li>';
			foreach ( $this->departments as $index => $department ) :
				$block .= <<<HTML
<li class="department" data-department="{$department->term_id}">
	{$department->name}
</li>
HTML;
			endforeach;
			$block .= '</ul>';

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to get list of departments employees.
	 *
	 * @return string
	 */
	private function get_all_employees() {
		$block = null;

		while ( have_posts() ) {
			the_post();
			$employee_name           = get_the_title();
			$employee_image          = ( get_the_post_thumbnail_url() ) ? get_the_post_thumbnail_url() : get_field( 'team_member_image_placeholder', 'option' );
			$employee_link           = get_the_permalink();
			$employee_position       = get_field( 'team_member_position' );
			$employee_department     = get_field( 'team_member_department' );
			$employee_location_id    = get_field( 'team_member_location' );
			$employee_location       = get_term( $employee_location_id );
			$employee_short_text     = get_the_excerpt();
			$employee_read_more_text = _x( 'Read More', 'team-members list', 'liquormaster' );
			$employee_read_more_icon = get_field( 'view_further_icon', 'option' );
			$employee_location_icon = get_field( 'location_icon', 'option' );
			$block                   .= <<<HTML
<div class="team-member active col-lg-6 d-flex align-items-stretch ftco-animate" data-department="{$employee_department}">
	<div class="blog-entry d-md-flex">
		<a href="{$employee_link}" class="block-20 img" style="background-image: url({$employee_image});">
		</a>
		<div class="text p-4 bg-light">
			<div class="meta">
				<p><span class="fa {$employee_location_icon}"></span> {$employee_location->name}</p>
			</div>
			<h3 class="heading mb-3">
				<a href="{$employee_link}">
					{$employee_name}
				</a>
			</h3>
			<div class="meta">
				<p>{$employee_position}</p>
			</div>
			<p>{$employee_short_text}</p>
			<a href="{$employee_link}" class="btn-custom">{$employee_read_more_text} <span
						class="fa {$employee_read_more_icon}"></span></a>
		</div>
	</div>
</div>
HTML;
		}

		return $block;
	}

}
