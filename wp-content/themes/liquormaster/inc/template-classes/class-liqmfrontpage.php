<?php

require_once 'class-liqmwebsiteblocks.php';

/**
 * Class for about us page render.
 *
 * @package WordPress
 */
class LiqmFrontPage extends LiqmWebsiteBlocks {

	/**
	 * Property to store post's custom fields values.
	 *
	 * @var mixed
	 */
	public $acf_post_fields;

	/**
	 * LiqmAboutUs constructor.
	 */
	public function __construct() {
		$this->acf_post_fields = get_field( 'front_page_blocks_website_blocks', get_the_ID() );
		parent::__construct();
	}

	/**
	 * Method to render list of blocks.
	 *
	 * @return bool
	 */
	public function render() {
		if ( ! empty( $this->acf_post_fields ) ) {
			foreach ( $this->acf_post_fields as $acf_post_field ) {
				echo call_user_func(
					array(
						$this,
						'get_' . $acf_post_field['acf_fc_layout'],
					),
					$acf_post_field['layout_data']
				);
			}

			return true;
		}

		return false;
	}

	/**
	 * Method to render informational block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_informational_blocks( $data ) {
		if ( ! empty( $data ) ) :
			$data_html = null;

			foreach ( $data as $index => $data_item ) :
				$data_html .= <<<HTML
<div class="col-md-4 d-flex">
    <div class="intro color-{$index} d-lg-flex w-100 ftco-animate">
        <div class="icon">
            <span class="{$data_item['intro_subblock_icon']}"></span>
        </div>
        <div class="text">
            <h2>{$data_item['intro_subblock_header']}</h2>
            <p>{$data_item['intro_subblock_text']}</p>
        </div>
    </div>
</div>
HTML;
			endforeach;

			$block = <<<HTML
    <section class="ftco-intro">
    	<div class="container">
    		<div class="row no-gutters">
				{$data_html}
    		</div>
    	</div>
    </section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render main info block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_main_info( $data ) {
		if ( ! empty( $data ) ) :
			$left_image = $right_image = null;

			if ( $data['main_info_layout_type'] == 'image_left' ) :
				$left_image = <<<HTML
<div class="col-md-6 img img-3 d-flex justify-content-center align-items-center" style="background-image: url({$data['main_info_image']});">
					</div>
HTML;
			endif;

			if ( $data['main_info_layout_type'] == 'image_right' ) :
				$right_image = <<<HTML
<div class="col-md-6 img img-3 d-flex justify-content-center align-items-center" style="background-image: url({$data['main_info_image']});">
					</div>
HTML;
			endif;

			$block = <<<HTML
<section class="ftco-section ftco-no-pb">
	<div class="container">
		<div class="row">
			{$left_image}
			<div class="col-md-6 wrap-about pl-md-5 ftco-animate py-5">
				<div class="heading-section">
        			<span class="subheading">{$data['main_info_subheading']}</span>
        				{$data['main_info_text']}
				        <p class="year">
				            <strong class="number" data-number="{$data['main_info_years_of_experience']['experience_years_number']}">0</strong>
				            <span>{$data['main_info_years_of_experience']['experience_text']}</span>
				        </p>
      			</div>
			</div>
			{$right_image}
		</div>
	</div>
</section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render alcohol types block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_alcohol_types( $data ) {
		if ( ! empty( $data ) ) :

			$block_content = null;
			foreach ( $data as $data_items ) :
				foreach ( $data_items as $data_item ) :
					$alcohol_type_image = get_term_meta( $data_item, 'thumbnail_id', true );
					$block_image        = wp_get_attachment_image_url( $alcohol_type_image );

					$alcohol_type_name = get_term( $data_item, 'product_cat' )->name;
					$alcohol_type_link = get_term_link( $data_item );

					$block_content .= <<<HTML
<div class="col-lg-2 col-md-4 ">
	<div class="sort w-100 text-center ftco-animate">
		<a href="{$alcohol_type_link}">
			<div class="img" style="background-image: url({$block_image});"></div>
			<h3>{$alcohol_type_name}</h3>
		</a>
	</div>
</div>
HTML;
				endforeach;
			endforeach;
			$block = <<<HTML
<section class="ftco-section">
			<div class="container">
				<div class="row">
					{$block_content}
				</div>
			</div>
		</section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_testimonials( $data ) {
		if ( ! empty( $data ) ) :
			$block_subheading        = __( 'Testimonial', 'liquormaster' );
			$testimonials_list_items = null;
			$testimonials_item_icon  = get_field( 'testimonials_item_icon', 'option' );

			foreach ( $data['testimonials_list'] as $testimonial ) :
				$testimonials_list_items .= <<<HTML
<div class="item">
	<div class="testimony-wrap py-4">
		<div class="icon d-flex align-items-center justify-content-center"><span class="fa {$testimonials_item_icon}"></div>
			<div class="text">
				<p class="mb-4">{$testimonial['testimonial_text']}</p>
				<div class="d-flex align-items-center">
					<div class="user-img" style="background-image: url({$testimonial['testimonial_author_image']})">
				</div>
				<div class="pl-3">
					<p class="name">{$testimonial['testimonial_author_name']}</p>
					<span class="position">{$testimonial['testimonial_author_position']}</span>
				</div>
			</div>
		</div>
	</div>
</div>
HTML;
			endforeach;
			$block = <<<HTML
<section class="ftco-section testimony-section img" style="background-image: url({$data['testimonials_background_image']});">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
          	<span class="subheading">{$block_subheading}</span>
            <h2 class="mb-3">{$data['testimonials_header_text']}</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              {$testimonials_list_items}
            </div>
          </div>
        </div>
      </div>
    </section>
HTML;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render offers on front page.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string
	 */
	protected function get_offers( $data ) {

		$recent_blog_read_more_icon = get_field( 'view_further_icon', 'option' );

		$offers_subheader = __( 'Our Delightful offerings', 'liquormaster' );
		$offers_header    = __( 'Tastefully Yours', 'liquormaster' );
		$offers_view_all  = __( 'View All Products', 'liquormaster' );

		$shop_permalink = wc_get_page_permalink( 'shop' );

		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => 8,
			'orderby'        => 'rand',
		);

		$products_list_data = new WP_Query( $args );

		$products_list = '';
		if ( $products_list_data->have_posts() ) :
			while ( $products_list_data->have_posts() ) :
				$products_list_data->the_post();
				ob_start();
				wc_get_template_part( 'content', 'product' );
				$products_list .= ob_get_clean();
			endwhile;
			wp_reset_query();
		endif;

		$block = <<<HTML
<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center pb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">{$offers_subheader}</span>
				<h2>{$offers_header}</h2>
			</div>
		</div>
		<div class="woocommerce woocommerce-front">
			<div class="products row">
				{$products_list}
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-4">
				<a href="{$shop_permalink}" class="btn btn-primary d-block">{$offers_view_all} <span class="fa {$recent_blog_read_more_icon}"></span></a>
			</div>
		</div>
	</div>
</section>
HTML;

		return $block;
	}

	/**
	 * Method to render recent blog according to post type blog settings.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string|null
	 */
	protected function get_recent_blog( $data ) {
		if ( ! empty( $data ) ) :
			$args        = array(
				'post_type'      => $data['post_type_recent_blog'],
				'posts_per_page' => $data['posts_to_render'],
			);
			$recent_blog = new WP_Query( $args );

			if ( $recent_blog->have_posts() ) :
				$recent_blog_subheading            = $data['recent_blog_subheading'];
				$recent_blog_heading               = $data['recent_blog_heading'];
				$recent_block_item_continue_button = _x( 'Continue', 'front page recent blog', 'liquormaster' );

				$block = <<<HTML
<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center mb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">{$recent_blog_subheading}</span>
				<h2>{$recent_blog_heading}</h2>
			</div>
		</div>
		<div class="row d-flex">
HTML;
				while ( $recent_blog->have_posts() ) :
					$recent_blog->the_post();
					$recent_blog_post_image     = get_the_post_thumbnail_url();
					$recent_blog_post_title     = get_the_title();
					$recent_blog_post_date      = get_the_date( 'j F Y' );
					$recent_blog_post_excerpt   = get_the_excerpt();
					$recent_blog_post_permalink = get_the_permalink();
					$recent_blog_read_more_icon = get_field( 'view_further_icon', 'option' );
					$recent_blog_icons          = get_field( 'blog_icons', 'option' );
					$block                      .= <<<HTML
<div class="col-lg-6 d-flex align-items-stretch ftco-animate">
	<div class="blog-entry d-flex">
		<a href="{$recent_blog_post_permalink}" class="block-20 img" style="background-image: url({$recent_blog_post_image});">
		</a>
		<div class="text p-4 bg-light">
			<div class="meta">
				<p><span class="fa {$recent_blog_icons['blog_icon_calendar']}"></span> {$recent_blog_post_date}</p>
			</div>
			<h3 class="heading mb-3"><a href="{$recent_blog_post_permalink}">{$recent_blog_post_title}</a></h3>
			<p>{$recent_blog_post_excerpt}</p>
			<a href="{$recent_blog_post_permalink}" class="btn-custom">{$recent_block_item_continue_button} <span class="fa {$recent_blog_read_more_icon}"></span></a>

		</div>
	</div>
</div>
HTML;
				endwhile;
				$block .= <<<HTML
		</div>
	</div>
</section>
HTML;
				wp_reset_postdata();
			endif;

			return $block;
		endif;

		return false;
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 *
	 * @return false|string|void
	 */
	protected function get_counters_list( $data ) {
	}
}
