<?php

/**
 * Class for about us page render.
 *
 * @package WordPress
 */
class LiqmWebsiteBlocks {

	/**
	 * Property storing all option fields belonging to website option.
	 *
	 * @var array|false
	 */
	public $acf_option_fields;

	/**
	 * Property to store URI of theme directory.
	 *
	 * @var string
	 */
	public $template_directory;

	/**
	 * LiqmAboutUs constructor.
	 */
	public function __construct() {
		$this->acf_option_fields  = get_fields( 'option' );
		$this->template_directory = THEME_DIR_URI;
	}

	/**
	 * Method to render list of blocks.
	 *
	 * @return void
	 */
	public function render() {
	}

	/**
	 * Method to render informational block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_informational_blocks( $data ) {
	}

	/**
	 * Method to render main info block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_main_info( $data ) {
	}

	/**
	 * Method to render alcohol types block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_alcohol_types( $data ) {
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_testimonials( $data ) {
	}

	/**
	 * Method to render testimonials block of about us page template.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_counters_list( $data ) {
	}

	/**
	 * Method to render shop offers.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_offers( $data ) {
	}

	/**
	 * Method to render recent blog.
	 *
	 * @param array $data array of data to iterate through.
	 */
	protected function get_recent_blog( $data ) {
	}
}
