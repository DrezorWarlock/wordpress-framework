<?php

/**
 * Class for header render.
 *
 * @package WordPress
 */
class LiqmThemeHeader {

	/**
	 * Property storing all option fields belonging to website option.
	 *
	 * @var array|false
	 */
	private $acf_fields;

	/**
	 * Property to store URI of theme directory.
	 *
	 * @var string
	 */
	private $template_directory;

	/**
	 * Property to store current page id for later use.
	 *
	 * @var false|int
	 */
	private $page_id;

	/**
	 * ThemeHeader constructor.
	 */
	public function __construct() {
		$this->acf_fields         = get_fields( 'option' );
		$this->template_directory = THEME_DIR_URI;
		$this->page_id            = get_the_ID();
	}

	/**
	 * Method to get wrap block of header.
	 *
	 * @return string
	 */
	public function get_header_wrap(): string {
		$contact_phone = null;
		$contact_email = null;
		$social_links  = null;

		if ( ! empty( $this->get_contact_data( 'contact_phone', 'contact_phone' ) ) ) :
			$contact_phone = <<<HTML
<a href="tel:{$this->get_contact_data( 'contact_phone', 'contact_phone' )}" class="mr-2">
	<span class="fa {$this->get_contact_data( 'contact_phone', 'contact_phone_icon' )} mr-1"></span>
	{$this->get_contact_data( 'contact_phone', 'contact_phone' )}
</a>
HTML;
		endif;

		if ( ! empty( $this->get_contact_data( 'contact_email', 'contact_email' ) ) ) :
			$contact_email = <<<HTML
<a href="tel:{$this->get_contact_data( 'contact_email', 'contact_email' )}" class="mr-2">
	<span class="fa {$this->get_contact_data( 'contact_email', 'contact_email_icon' )} mr-1"></span>
	{$this->get_contact_data( 'contact_email', 'contact_email' )}
</a>
HTML;
		endif;

		if ( ! empty( $this->get_social_links() ) ) :
			$social_links = $this->get_social_links();
		endif;

		$block = <<<HTML
<div class="wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-6 d-flex align-items-center">
				<p class="mb-0 phone pl-md-2">
					{$contact_phone}
					{$contact_email}
				</p>
			</div>
			<div class="col-md-6 d-flex justify-content-md-end">
				<div class="social-media mr-4">
					<p class="mb-0 d-flex">
						{$social_links}
					</p>
				</div>
				{$this->get_account_menu()}
			</div>
		</div>
	</div>
</div>
HTML;

		return $block;
	}

	/**
	 * Method to get header navigation block.
	 *
	 * @return string
	 */
	public function get_header_nav(): string {
		$mobile_menu_label = __( 'Menu', 'liquormaster' );

		$logo = null;

		ob_start();
		woocommerce_mini_cart();
		$mini_cart = ob_get_clean();

		$cart_url      = wc_get_cart_url();
		$cart_url_text = _x( 'View all', 'mini-cart', 'liquormaster' );

		if ( ! empty( $this->get_logo() ) ) :
			$logo = <<<HTML
<a class="navbar-brand" href="{$this->get_home_url()}">{$this->get_logo()}</a>
HTML;
		endif;
		$wishlist_url = YITH_WCWL()->get_wishlist_url() . '=view';

		global $yith_woocompare;

		$compare_count = count( $yith_woocompare->obj->products_list );
		if ( ! empty( YITH_WCWL()->get_current_user_wishlists() ) ) {
			$wishlist_url .= '&wishlist_id=' . YITH_WCWL()->get_current_user_wishlists()[0]->get_token();
		}

		$block = <<<HTML
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	<div class="container">
		{$logo}
		<div class="order-lg-last btn-group">
			<a href="{$wishlist_url}" target="_blank" class="btn-cart btn-wishlist dropdown-toggle dropdown-toggle-split"
			   aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-heart" aria-hidden="true"></i>
				<div id="total-items-wishlist" class="d-flex justify-content-center align-items-center"><small></small></div>
			</a>
			<a href="#" target="_blank" class="btn-cart btn-compare yith-woocompare-open dropdown-toggle dropdown-toggle-split"
			   aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-exchange" aria-hidden="true"></i>
				<div id="total-items-compare" class="d-flex justify-content-center align-items-center"><small></small></div>
			</a>
			<a href="#" class="btn-cart dropdown-toggle dropdown-toggle-split" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-shopping-bag" aria-hidden="true"></i>
				<div id="total-items-cart" class="d-flex justify-content-center align-items-center"><small></small></div>
			</a>
			<div id="mini-cart-top" class="dropdown-menu dropdown-menu-right">
				{$mini_cart}
				<a class="dropdown-item text-center btn-link d-block w-100 view-all-cart" href="{$cart_url}">
					{$cart_url_text}
					<span class="ion-ios-arrow-round-forward"></span>
				</a>
			</div>
		</div>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
				aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="oi oi-menu"></span> {$mobile_menu_label}
		</button>
		{$this->get_primary_menu()}
	</div>
</nav>
<!-- END nav -->
HTML;

		return $block;
	}

	/**
	 * Method to return template part depending on current page.
	 *
	 * @return mixed
	 */
	public function get_header_hero() {
		$version = ( is_front_page() ) ? 'front' : 'site';

		if ( ! empty( $this->get_page_title() ) ) {
			$args['page_title'] = $this->get_page_title();
		} else {
			$args['page_title'] = __( 'Placeholder Title', 'liquormaster' );
		}

		return get_template_part( 'template-parts/header-parts/hero', $version, $args );

	}

	/**
	 * Method to contain logic of getting title depending on page type.
	 *
	 * @return string|void
	 */
	private function get_page_title(): string {
		$page_title = get_the_title( $this->page_id );

		if ( is_home() ) {
			$page_title = get_the_title( get_option( 'page_for_posts' ) );
		}

		if ( is_archive() ) {
			$page_title = get_the_archive_title();
		}

		if ( is_search() ) {
			$page_title = __( 'Search', 'liquormaster' );
		}

		if ( is_404() ) {
			$page_title = get_the_title( get_field( '404_page', 'option' ) );
		}

		return $page_title;
	}

	/**
	 * Method to get assembled list of social media links and icons.
	 *
	 * @return false|string
	 */
	private function get_social_links(): string {
		if ( ! empty( $this->acf_fields['header_social_icons'] ) ) :
			$social_links = '';
			foreach ( $this->acf_fields['header_social_icons'] as $social_icon ) :
				$social_links .= "<a href='" . $social_icon['social_icon_link'] . "' class='d-flex align-items-center justify-content-center'><span class='fa " . $social_icon['social_icon'] . "'><i class='sr-only'>Facebook</i></span></a>";
			endforeach;

			return $social_links;
		endif;

		return false;
	}

	/**
	 * Method to get cart icon of header.
	 *
	 * @return false|mixed
	 */
	private function get_cart_icon(): string {
		if ( ! empty( $this->acf_fields['header_cart_icon'] ) ) {
			return $this->acf_fields['header_cart_icon'];
		}

		return false;
	}

	/**
	 * Method to get home url since regular functions can't be used with heredoc syntax.
	 *
	 * @return string|void
	 */
	private function get_home_url(): string {
		return home_url( '/' );
	}

	/**
	 * Method to get website logo. Made just because was explicitely requested in homework.
	 *
	 * @return mixed
	 */
	private function get_logo(): string {
		if ( ! empty( $this->acf_fields['logo_text'] ) ) {
			return $this->acf_fields['logo_text'];
		}

		return false;
	}

	/**
	 * Method to get contact data like contact phone or email.
	 *
	 * @param null $group_name name of the field group.
	 * @param null $field_name name of the field within group.
	 *
	 * @return false|mixed|string
	 */
	private function get_contact_data( $group_name = null, $field_name = null ): string {
		if ( null === $group_name || null === $field_name ) {
			return false;
		}

		return $this->acf_fields[ 'header_' . $group_name . '_group' ][ $field_name ];
	}

	/**
	 * Method to get navigation menu.
	 *
	 * @return false|string|void
	 */
	private function get_primary_menu() {
		$menu = wp_nav_menu(
			array(
				'theme_location'  => 'primary_menu',
				'container'       => 'div',
				'container_class' => 'collapse navbar-collapse',
				'container_id'    => 'ftco-nav',
				'menu_class'      => 'navbar-nav ml-auto',
				'echo'            => false,
			)
		);

		return $menu;
	}

	/**
	 * Method to get account menu.
	 *
	 * @return false|string|void
	 */
	private function get_account_menu() {
		$menu = wp_nav_menu(
			array(
				'theme_location'  => 'account_menu',
				'container'       => 'div',
				'container_class' => 'reg',
				'container_id'    => '',
				'menu_class'      => 'mb-0',
				'echo'            => false,
			)
		);

		return $menu;
	}
}
