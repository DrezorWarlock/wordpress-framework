<?php
/**
 *
 * Collection of sidebars
 *
 * @package WordPress
 */

add_action( 'widgets_init', 'liqm_register_widgets' );
/**
 * Callback to register sidebars for theme.
 */
function liqm_register_widgets() {

	register_sidebar(
		array(
			'name'          => __( 'Footer Left' ),
			'id'            => 'footer_left',
			'before_widget' => '',
			'after_widget'  => '',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer Left Middle' ),
			'id'            => 'footer_left_middle',
			'before_title'  => '<h2 class="ftco-heading-2">',
			'after_title'   => '</h2>',
			'before_widget' => '',
			'after_widget'  => '',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer Middle' ),
			'id'            => 'footer_middle',
			'before_title'  => '<h2 class="ftco-heading-2">',
			'after_title'   => '</h2>',
			'before_widget' => '',
			'after_widget'  => '',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer Right Middle' ),
			'id'            => 'footer_right_middle',
			'before_title'  => '<h2 class="ftco-heading-2">',
			'after_title'   => '</h2>',
			'before_widget' => '',
			'after_widget'  => '',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer Right' ),
			'id'            => 'footer_right',
			'before_title'  => '<h2 class="ftco-heading-2">',
			'after_title'   => '</h2>',
			'before_widget' => '<div class="block-23 mb-3">',
			'after_widget'  => '</div>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Nothing Found Search Form' ),
			'id'            => 'nothing_found_search',
			'before_widget' => '<div class="sidebar-box">',
			'after_widget'  => '</div>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Search Form' ),
			'id'            => 'search_form',
			'before_widget' => '<div class="sidebar-box">',
			'after_widget'  => '</div>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Sidebar' ),
			'id'            => 'content_sidebar',
			'before_widget' => '<div class="sidebar-box">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'WooCommerce Shop Sidebar' ),
			'id'            => 'wc_shop_sidebar',
			'before_widget' => '<div class="sidebar-box">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		)
	);
}
