<?php
/**
 * WooCommerce filters collection
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

add_filter( 'woocommerce_format_sale_price', 'liqm_custom_on_sale_price_format', 10, 3 );

if ( ! function_exists( 'liqm_custom_on_sale_price_format' ) ) {
	function liqm_custom_on_sale_price_format( $price, $regular_price, $sale_price ) {
		if ( is_product() ) {

			$price = '<p><span class="price price-sale"><span>' . ( is_numeric( $regular_price ) ? wc_price( $regular_price ) : $regular_price ) . '</span></span> <span class="price"><span>' . ( is_numeric( $sale_price ) ? wc_price( $sale_price ) : $sale_price ) . '</span></span></p>';

		} else {

			$price = '<p class="mb-0"><span class="price price-sale">' . ( is_numeric( $regular_price ) ? wc_price( $regular_price ) : $regular_price ) . '</span> <span class="price">' . ( is_numeric( $sale_price ) ? wc_price( $sale_price ) : $sale_price ) . '</span></p>';

		}

		return $price;
	}
}

add_filter( 'wc_price', 'liqm_custom_price_html', 10, 3 );

if ( ! function_exists( 'liqm_custom_price_html' ) ) {
	function liqm_custom_price_html( $return, $price, $args ) {
		$negative = $price < 0;

		$return = ( $negative ? '-' : '' ) . sprintf( $args['price_format'], get_woocommerce_currency_symbol( $args['currency'] ), $price );

		return $return;
	}
}

add_filter( 'woocommerce_get_availability', 'liqm_custom_in_stock_text', 10, 2 );

if ( ! function_exists( 'liqm_custom_in_stock_text' ) ) {
	function liqm_custom_in_stock_text( $array, $product ) {

		$stock_management     = $product->get_manage_stock();
		$stock_status         = $product->get_stock_status();
		$stock_quantity       = $product->get_stock_quantity();
		$stock_status_display = array(
			'instock'     => _x( 'In Stock', 'stock_quantity', 'liquormaster' ),
			'outofstock'  => _x( 'Out of Stock', 'stock_quantity', 'liquormaster' ),
			'onbackorder' => _x( $array['availability'], 'stock_quantity', 'liquormaster' ),
		);

		$array['availability'] = _x( $stock_status_display[ $stock_status ], 'stock_quantity', 'liquormaster' );


		$available_on_backorder = '';
		if ( $stock_management && $stock_quantity > 0 ) {
			$array['availability'] = sprintf( _nx( '%d piece available %s', '%d pieces available %s', $product->get_stock_quantity(), 'stock_quantity', 'liquormaster' ), $stock_quantity, $available_on_backorder );
		} elseif ( $stock_quantity == 0 ) {
			$array['availability'] = _x( $stock_status_display[ $stock_status ], 'stock_quantity', 'liquormaster' );
		} else {
			if ( 'available-on-backorder' == strtolower( $array['class'] ) ) {
				$available_on_backorder = strtolower( 'but available on backorder' );
				$stock_quantity         = 0;
			}

			$array['availability'] = sprintf( _nx( '%d piece available %s', '%d pieces available %s', $product->get_stock_quantity(), 'stock_quantity', 'liquormaster' ), $stock_quantity, $available_on_backorder );
		}

		return $array;
	}
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'custom_add_to_cart_text', 100 );

if ( ! function_exists( 'custom_add_to_cart_text' ) ) {
	function custom_add_to_cart_text( $text ) {
		$text = __( 'Add to Cart', 'liquormaster' );

		return $text;
	}
}

add_filter( 'woocommerce_reviews_title', 'liqm_custom_reviews_title', 10, 2 );

if ( ! function_exists( 'liqm_custom_reviews_title' ) ) {
	function liqm_custom_reviews_title( $reviews_title, $count ) {
		$reviews_title = sprintf( esc_html( _n( '%1$s review', '%1$s reviews', $count, 'woocommerce' ) ), esc_html( $count ) );

		return $reviews_title;
	}
}

add_filter( 'woocommerce_product_tabs', 'liqm_custom_tabs' );
function liqm_custom_tabs( $tabs ) {

	global $product;

	if ( $product->is_type( 'variable' ) ) {
		$tabs['homework_tab'] = array(
			'title'    => __( 'Homework Tab', 'woocommerce' ),
			'priority' => 25,
			'callback' => 'liqm_homework_tab_content'
		);
	}


	return $tabs;
}

function liqm_homework_tab_content() {
	$post_id = get_field( 'homework_custom_tab', 'option' );

	echo get_post_field( 'post_content', $post_id );
}

add_filter( 'woocommerce_cart_item_thumbnail', 'liqm_custom_mini_cart_item_thumbnail', 10, 2 );

if ( ! function_exists( 'liqm_custom_mini_cart_item_thumbnail' ) ) {
	function liqm_custom_mini_cart_item_thumbnail( $image, $cart_item ) {
		$image = '<div class="img" style="background-image: url(' . wp_get_attachment_image_url( $cart_item['data']->get_image_id() ) . ');"></div>';

		return $image;
	}
}

add_filter( 'woocommerce_widget_cart_item_quantity', 'liqm_custom_mini_cart_item_info', 10, 2 );

if ( ! function_exists( 'liqm_custom_mini_cart_item_info' ) ) {
	function liqm_custom_mini_cart_item_info( $html, $cart_item ) {
		$quantity = ( $cart_item['quantity'] > 0 && $cart_item['quantity'] < 10 ) ? '0' . $cart_item['quantity'] : $cart_item['quantity'];
		$html     = sprintf( '%s %s', '<a href="#" class="price">' . get_woocommerce_currency_symbol() . ' ' . $cart_item['data']->get_price() . '</a>', '<span class="quantity ml-3">' . _x( 'Quantity', 'mini-cart', 'liquormaster' ) . ': ' . $quantity . '</span>' );

		return $html;
	}
}


add_filter( 'woocommerce_add_to_cart_fragments', 'liqm_update_wc_mini_cart' );

if ( ! function_exists( 'liqm_update_wc_mini_cart' ) ) {
	function liqm_update_wc_mini_cart( $fragments ) {
		$fragments['div#total-items-cart small'] = '<small>' . WC()->cart->get_cart_contents_count() . '</small>';

		ob_start();
		woocommerce_mini_cart();
		$mini_cart = ob_get_clean();

		$fragments['div#mini-cart-top ul.woocommerce-mini-cart, div#mini-cart-top .woocommerce-mini-cart__empty-message'] = $mini_cart;

		return $fragments;
	}
}

add_filter( 'woocommerce_states', 'liqm_ukrainian_regions' );

if ( ! function_exists( 'liqm_ukrainian_regions' ) ) {

	function liqm_ukrainian_regions( $states ) {

		$states['UA'] = array(
			'CK'      => _x( 'Cherkasy Oblast', 'ukrainian regions', 'liquormaster' ),
			'CH'      => _x( 'Chernihiv Oblast', 'ukrainian regions', 'liquormaster' ),
			'CV'      => _x( 'Chernivtsi Oblast', 'ukrainian regions', 'liquormaster' ),
			'CR'      => _x( 'Crimea', 'ukrainian regions', 'liquormaster' ),
			'DP'      => _x( 'Dnipro Oblast', 'ukrainian regions', 'liquormaster' ),
			'DT'      => _x( 'Donetsk Oblast', 'ukrainian regions', 'liquormaster' ),
			'IF'      => _x( 'Ivano-Frankivsk Oblast', 'ukrainian regions', 'liquormaster' ),
			'KK'      => _x( 'Kharkiv Oblast', 'ukrainian regions', 'liquormaster' ),
			'KS'      => _x( 'Kherson Oblast', 'ukrainian regions', 'liquormaster' ),
			'KM'      => _x( 'Khmelnytskyi Oblast', 'ukrainian regions', 'liquormaster' ),
			'KV'      => _x( 'Kyiv Oblast', 'ukrainian regions', 'liquormaster' ),
			'KH'      => _x( 'Kirovohrad Oblast', 'ukrainian regions', 'liquormaster' ),
			'LH'      => _x( 'Luhanks Oblast', 'ukrainian regions', 'liquormaster' ),
			'LV'      => _x( 'Lviv Oblast', 'ukrainian regions', 'liquormaster' ),
			'MY'      => _x( 'Mykolaiv Oblast', 'ukrainian regions', 'liquormaster' ),
			'OD'      => _x( 'Odessa Oblast', 'ukrainian regions', 'liquormaster' ),
			'PL'      => _x( 'Poltava Oblast', 'ukrainian regions', 'liquormaster' ),
			'Poltava' => _x( 'Poltava', 'ukrainian regions', 'liquormaster' ),
			'RV'      => _x( 'Rivne Oblast', 'ukrainian regions', 'liquormaster' ),
			'SM'      => _x( 'Sumy Oblast', 'ukrainian regions', 'liquormaster' ),
			'TP'      => _x( 'Ternopil Oblast', 'ukrainian regions', 'liquormaster' ),
			'VI'      => _x( 'Vinnytsia Oblast', 'ukrainian regions', 'liquormaster' ),
			'VO'      => _x( 'Volyn Oblast', 'ukrainian regions', 'liquormaster' ),
			'ZK'      => _x( 'Zakarpattia Oblast', 'ukrainian regions', 'liquormaster' ),
			'ZP'      => _x( 'Zaporizhzhia Oblast', 'ukrainian regions', 'liquormaster' ),
			'ZT'      => _x( 'Zhytomyr Oblast', 'ukrainian regions', 'liquormaster' ),
		);

		return $states;
	}
}

add_filter( 'woocommerce_shipping_package_name', 'liqm_shipping_package_name', 10, 3 );

if ( ! function_exists( 'liqm_shipping_package_name' ) ) {
	function liqm_shipping_package_name( $html, $i, $package ) {
		$html = sprintf( _nx( 'Delivery', 'Delivery %d', ( $i + 1 ), 'shipping packages', 'woocommerce' ), ( $i + 1 ), $i, $package );

		return $html;
	}
}

add_filter( 'woocommerce_default_address_fields', 'liqm_custom_address_fields_priority', 10 );

if ( ! function_exists( 'liqm_custom_address_fields_priority' ) ) {
	function liqm_custom_address_fields_priority( $fields ) {
		unset( $fields['company'] );
		unset( $fields['country'] );
		$fields['state']['priority'] = 35;

		return $fields;
	}
}
add_filter( 'woocommerce_update_order_review_fragments', 'liqm_update_order' );

if ( ! function_exists( 'liqm_update_order' ) ) {
	function liqm_update_order( $fragments ) {
		ob_start();
		wc_get_template_part( 'checkout/review-order' );
		$review_order = ob_get_clean();

		$fragments['div.checkout-review-order-wrapper'] = $review_order;

		ob_start();
		wc_get_template_part( 'checkout/payment' );
		$payment = ob_get_clean();

		$fragments['div.checkout-payment-wrapper'] = $payment;

		return $fragments;
	}
}

add_filter( 'woocommerce_order_button_html', 'liqm_place_order_button_html' );

if ( ! function_exists( 'liqm_place_order_button_html' ) ) {
	function liqm_place_order_button_html( $html ) {
		$order_button_text = __( 'Place an order', 'liquormaster' );

		$html = '<button type="submit" class="btn btn-primary py-3 px-4 alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>';

		return $html;
	}
}

add_filter( 'woocommerce_get_terms_and_conditions_checkbox_text', 'liqm_custom_tac_text' );

if ( ! function_exists( 'liqm_custom_tac_text' ) ) {
	function liqm_custom_tac_text( $text ) {

		$text = sprintf( __( 'I have read and accept %s', 'woocommerce' ), '[terms]' );

		return $text;
	}
}


add_filter( 'woocommerce_account_menu_items', 'liqm_cart_content_tab', 40 );
function liqm_cart_content_tab( $menu_links ) {

	$menu_links = array_slice( $menu_links, 0, 1, true )
	              + array( 'cart-content' => 'Cart Content' )
	              + array_slice( $menu_links, 1, null, true );

	return $menu_links;

}

/*
 * Step 2. Register Permalink Endpoint
 */
add_action( 'init', 'liqm_add_endpoint' );
function liqm_add_endpoint() {

	// WP_Rewrite is my Achilles' heel, so please do not ask me for detailed explanation
	add_rewrite_endpoint( 'cart-content', EP_PAGES );

}

/*
 * Step 3. Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint
 */
add_action( 'woocommerce_account_cart-content_endpoint', 'liqm_cart_content_endpoint_content' );
function liqm_cart_content_endpoint_content() {
	$items = WC()->cart->get_cart();

	$product_html = '';
	if ( ! empty( $items ) ) :
		foreach ( $items as $key => $item ) {

			$_product              = wc_get_product( $item['data']->get_id() );
			$product_title         = $item['data']->get_name();
			$product_quantity_text = __( 'Quantity', 'liquormaster' );
			$product_quantity      = $item['quantity'];
			$product_price         = $_product->get_price_html();
			$product_permalink     = get_permalink( $item['data']->get_id() );
			$product_thumbnail     = wp_get_attachment_image_url( $_product->get_image_id(), 'full' );
			$cart_url              = wc_get_cart_url();

			$product_html .= <<<HTML
<div class="col-md-4 d-flex">
	<div class="product">
		<div class="img d-flex align-items-center justify-content-center" style="background-image: url({$product_thumbnail});">
			<div class="desc">
				<div class="meta-prod d-flex">
					<a href="{$cart_url}" class="d-flex align-items-center justify-content-center"><span class="flaticon-shopping-bag"></span></a>
					<a href="{$product_permalink}" class="d-flex align-items-center justify-content-center"><span class="flaticon-visibility"></span></a>
				</div>
			</div>
		</div>
		<div class="text text-center">
			<div class="quantity category">
				{$product_quantity_text}: {$product_quantity}
			</div>
			<h2>
				{$product_title}
			</h2>
			{$product_price}
		</div>
	</div>
</div>
HTML;

		}
	else :
		$product_html = '<div class="no-products-cart">' . __( 'Sorry, there is no products in your cart', 'liquormaster' ) . '</div>';
	endif;
	$block = <<<HTML
<section class="ftco-section shop-block">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row mb-4">
					{$product_html}
				</div>
			</div>
		</div>
	</div>
</section>
HTML;

	echo $block;
}
