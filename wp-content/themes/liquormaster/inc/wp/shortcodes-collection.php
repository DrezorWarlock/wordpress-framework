<?php
/**
 * Collection of shortcodes
 *
 * @package WordPress
 */

add_shortcode( 'footer-left', 'liqm_footer_left' );

if ( ! function_exists( 'liqm_footer_left' ) ) {
	/**
	 * Footer left side specific shortcode callback.
	 *
	 * @return string
	 */
	function liqm_footer_left(): string {
		$home_url                 = home_url( '/' );
		$logo_text                = ( ! empty( get_field( 'logo_text', 'option' ) ) ) ? get_field( 'logo_text', 'option' ) : null;
		$footer_left_block_fields = get_field( 'footer_left_block', 'option' );
		$inspiration_quote        = ( ! empty( $footer_left_block_fields['footer_left_block_inspirational_quote'] ) ) ? $footer_left_block_fields['footer_left_block_inspirational_quote'] : null;
		$social_links             = '';

		if ( array_filter( $footer_left_block_fields['footer_left_block_social_links'] ) ) {
			foreach ( $footer_left_block_fields['footer_left_block_social_links'] as $social_link ) {
				$social_links .= "<li class='ftco-animate'><a href='{$social_link['footer_left_block_social_link']}'><span class='fa {$social_link['footer_left_block_social_link_icon']}'></span></a></li>";
			}
		}
		$block = <<<HTML
<div class="ftco-footer-widget mb-4">
	<h2 class="ftco-heading-2 logo"><a href="{$home_url}">{$logo_text}</a></h2>
	<p>{$inspiration_quote}</p>
	<ul class="ftco-footer-social list-unstyled mt-2">
		{$social_links}
	</ul>
</div>
HTML;

		return $block;
	}
}

add_shortcode( 'footer-menu', 'liqm_footer_get_menu' );

if ( ! function_exists( 'liqm_footer_get_menu' ) ) {
	/**
	 * Shortcode callback to render menu of specific theme location in footer.
	 *
	 * @param array $args array to hold arguments passed through shortcode parameters.
	 *
	 * @return false|string|void
	 */
	function liqm_footer_get_menu( array $args ): string {
		return wp_nav_menu(
			array(
				'theme_location' => $args['theme_location'],
				'menu_class'     => 'list-unstyled',
				'echo'           => false,
				'walker'         => new Liqm_Footer_Nav_Menu_Walker(),
			)
		);
	}
}

add_shortcode( 'footer-right', 'liqm_footer_right' );

if ( ! function_exists( 'liqm_footer_right' ) ) {
	/**
	 * Footer right side specific shortcode callback.
	 *
	 * @return string
	 */
	function liqm_footer_right(): string {
		$footer_right_block_items = get_field( 'footer_right_block', 'option' )['footer_right_block_items'];

		$footer_right_block_html = '';

		if ( array_filter( $footer_right_block_items ) ) {
			foreach ( $footer_right_block_items as $index => $footer_right_block_field_item ) {
				$link_type = '';

				if ( 1 === $index ) {
					$contact_link = 'tel:' . str_replace( ' ', '', $footer_right_block_field_item['footer_right_block_text'] );
				} elseif ( 2 === $index ) {
					$contact_link = 'mailto:' . trim( $footer_right_block_field_item['footer_right_block_text'] );
				}

				if ( 0 === $index ) {
					$footer_right_block_html .= "<li><span class='icon fa {$footer_right_block_field_item['footer_right_block_icon']} marker'></span><span class='text'>{$footer_right_block_field_item['footer_right_block_text']}</span>";
				} else {
					$footer_right_block_html .= "<li><a href={$contact_link}>
<span class='icon fa {$footer_right_block_field_item['footer_right_block_icon']} '></span><span class='text'>{$footer_right_block_field_item['footer_right_block_text']}</span>
</a>
";
				}

			}
		}

		$block = <<<HTML
<div class="block-23 mb-3">
	<ul>
		{$footer_right_block_html}
	</ul>
</div>
HTML;

		return $block;
	}
}

add_shortcode( 'custom_recent_posts', 'liqm_custom_recent_blog' );

if ( ! function_exists( 'liqm_custom_recent_blog' ) ) {
	/**
	 * Function to get list of recent blog entries.
	 *
	 * @param array $args array containing shortcode arguments.
	 *
	 * @return string|null
	 */
	function liqm_custom_recent_blog( array $args ): string {
		$args = array(
			'post_type'      => $args['post_type'],
			'posts_per_page' => $args['posts_per_page'],
		);

		$custom_recent_posts = new WP_Query( $args );

		if ( $custom_recent_posts->have_posts() ) {
			$output = null;
			while ( $custom_recent_posts->have_posts() ) {
				$custom_recent_posts->the_post();

				$post_thumbnail = get_the_post_thumbnail_url();
				$title          = get_the_title();
				$date           = get_the_date( 'M. d, Y' );
				$author         = get_the_author();
				$comments_count = get_comment_count( get_the_ID() );
				$permalink      = get_the_permalink();
				$item_icons     = get_field( 'blog_icons', 'option' );

				$output .= <<<HTML
<div class="block-21 mb-4 d-flex">
	<a class="blog-img mr-4" style="background-image: url({$post_thumbnail});"></a>
	<div class="text">
	  <h3 class="heading"><a href="{$permalink}">{$title}</a></h3>
	  <div class="meta">
	    <div><a href="{$permalink}"><span class="fa {$item_icons['blog_icon_calendar']}"></span> {$date}</a></div>
	    <div><a href="{$permalink}"><span class="fa {$item_icons['blog_icon_author']}"></span> {$author}</a></div>
	    <div><a href="{$permalink}"><span class="fa {$item_icons['blog_icon_comments']}"></span> {$comments_count['total_comments']}</a></div>
	  </div>
	</div>
</div>
HTML;
			}
			wp_reset_postdata();
		}

		return $output;
	}
}

add_shortcode( 'liqm-wc-categories-sidebar', 'liqm_wc_categories' );

if ( ! function_exists( 'liqm_wc_categories' ) ) {
	/**
	 * Shortcode callback to render list of products top level categories.
	 *
	 * @param array $args array to hold arguments passed through shortcode parameters.
	 *
	 * @return false|string|void
	 */
	function liqm_wc_categories( array $args ): string {
		$args_list = array( 'exclude' => 29 );

		if ( !empty( $args['arguments'] ) ) {
			$argument = array();
			foreach ( explode( ',', $args['arguments'] ) as $arg_item ) {
				$argument                  = array_slice( explode( '=', $arg_item ), 0, 2 );
				$args_list[ $argument[0] ] = $argument[1];
			}
		}

		$product_cats = get_terms( 'product_cat', $args_list );

		$title      = _x( $args['title'], 'product_categories_widget', 'liquormaster' );
		$categories = '';
		if ( ! empty( $product_cats ) ) {
			foreach ( $product_cats as $product_cat ) {
				$categories .= '<li><a href="' . get_term_link( $product_cat->term_id ) . '">' . $product_cat->name . '<span class="fa fa-chevron-right"></span></a></li>';
			}
		}
		$block = <<<HTML
<div class="categories">
	<h3>{$title}</h3>
	<ul class="p-0">
		{$categories}
	</ul>
</div>
HTML;

		return $block;
	}
}
