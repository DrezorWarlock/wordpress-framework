<?php
/**
 * Collection of WooCommerce custom actions
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

add_action( 'woocommerce_before_shop_loop_item', 'liqm_template_loop_product_wrapper_open', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 5 );
add_action( 'woocommerce_review_before_comment_text', 'woocommerce_review_display_rating', 10 );
add_action( 'liqm_add_to_cart_custom_position', 'woocommerce_template_loop_add_to_cart', 10 );

if ( ! function_exists( 'liqm_template_loop_product_wrapper_open' ) ) {
	/**
	 * Function to render opening tag for product item of WooCommerce product loop
	 */
	function liqm_template_loop_product_wrapper_open() {
		$wrapper_open = '<div class="product">';

		echo $wrapper_open;
	}
}

add_action( 'woocommerce_after_shop_loop_item', 'liqm_template_loop_product_wrapper_close', 10 );

if ( ! function_exists( 'liqm_template_loop_product_wrapper_close' ) ) {
	/**
	 * Function to render closing tag for product item of WooCommerce product loop
	 */
	function liqm_template_loop_product_wrapper_close() {
		$wrapper_close = '</div>';

		echo $wrapper_close;
	}
}

add_action( 'woocommerce_before_shop_loop_item_title', 'liqm_template_loop_product_thumbnail_block', 10 );

if ( ! function_exists( 'liqm_template_loop_product_thumbnail_block' ) ) {
	/**
	 * Function to render product thumbnail of WooCommerce product loop
	 */
	function liqm_template_loop_product_thumbnail_block() {
		global $product;

		$product_id        = $product->get_ID();
		$product_type      = $product->get_type();
		$product_thumbnail = get_the_post_thumbnail_url( $product_id );
		$product_permalink = get_permalink( $product_id );

		$add_to_cart_button = '';
		if ( $product->is_type( 'simple' ) ) {
			ob_start();
			do_action( 'liqm_add_to_cart_custom_position' );
			$add_to_cart_button = ob_get_clean();
		}

		$add_to_wishlist_button = do_shortcode( '[yith_wcwl_add_to_wishlist product_id="' . $product_id . '" label="" link_classes="add_to_wishlist single_add_to_wishlist d-flex align-items-center justify-content-center"]' );

		$block = <<<HTML
<div class="img d-flex align-items-center justify-content-center" style="background-image: url({$product_thumbnail});">
	<div class="desc">
		<div class="meta-prod d-flex">
			{$add_to_cart_button}
			{$add_to_wishlist_button}
			<a href="{$product_permalink}" class="d-flex align-items-center justify-content-center"><span class="flaticon-visibility"></span></a>
		</div>
	</div>
</div>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_before_shop_loop_item_title', 'liqm_product_item_before_title', 15 );

if ( ! function_exists( 'liqm_product_item_before_title' ) ) {
	/**
	 * Function to fetch and render categories each product belongs to
	 */
	function liqm_product_item_before_title() {
		global $product;

		$product_terms_list = get_the_term_list( $product->get_ID(), 'product_cat', '', ', ', '' );

		ob_start();
		woocommerce_show_product_loop_sale_flash();
		$on_sale = ob_get_clean();

		$block = <<<HTML
<div class="text text-center">
{$on_sale}
<span class="category">{$product_terms_list}</span>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_after_shop_loop_item_title', 'liqm_product_item_after_title', 15 );

if ( ! function_exists( 'liqm_product_item_after_title' ) ) {
	/**
	 * Function to fetch and render categories each product belongs to
	 */
	function liqm_product_item_after_title() {
		$block = '</div>';

		echo $block;
	}
}

add_action( 'woocommerce_before_quantity_input_field', 'liqm_single_product_minus_quantity_control', 100 );

if ( ! function_exists( 'liqm_single_product_minus_quantity_control' ) ) {
	/**
	 * Function to fetch and render categories each product belongs to
	 */
	function liqm_single_product_minus_quantity_control() {
		$block = <<<HTML
<span class="input-group-btn mr-2">
	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
		<i class="fa fa-minus"></i>
	</button>
</span>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_after_quantity_input_field', 'liqm_single_product_plus_quantity_control', 100 );

if ( ! function_exists( 'liqm_single_product_plus_quantity_control' ) ) {
	/**
	 * Function to fetch and render categories each product belongs to
	 */
	function liqm_single_product_plus_quantity_control() {
		$block = <<<HTML
<span class="input-group-btn ml-2">
	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
		<i class="fa fa-plus"></i>
	</button>
</span>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_before_add_to_cart_quantity', 'liqm_single_product_quantity_control_open_wrapper', 100 );

if ( ! function_exists( 'liqm_single_product_quantity_control_open_wrapper' ) ) {
	function liqm_single_product_quantity_control_open_wrapper() {
		$block = <<<HTML
<div class="row mt-4">
	<div class="input-group col-md-6 d-flex mb-3">
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_after_add_to_cart_quantity', 'liqm_single_product_quantity_control_close_wrapper', 100 );

if ( ! function_exists( 'liqm_single_product_quantity_control_close_wrapper' ) ) {
	function liqm_single_product_quantity_control_close_wrapper() {
		global $product;
		$stock = wc_get_stock_html( $product ); // WPCS: XSS ok.
		$block = <<<HTML
	</div>
	{$stock}
</div>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_review_before', 'liqm_wc_comments_user_avatar', 10 );

if ( ! function_exists( 'liqm_wc_comments_user_avatar' ) ) {
	function liqm_wc_comments_user_avatar( $comment ) {
		$author_name = $comment->comment_author;

		if ( email_exists( $comment->comment_author_email ) ) {
			$author_id        = get_user_by( 'email', $comment->comment_author_email )->data->ID;
			$author_avatar_id = get_the_author_meta( 'liq_user_avatar', $author_id );
		}

		$author_avatar_url = ( ! empty( $author_avatar_id ) ) ? wp_get_attachment_image_url( $author_avatar_id ) :
			get_field( 'default_user_image', 'option' );

		$block = <<<HTML
<div class="user-img" style="background-image: url({$author_avatar_url})"></div>
HTML;

		echo $block;
	}
}

add_action( 'woocommerce_review_comment_text', 'liqm_review_display_comment_text', 10 );

if ( ! function_exists( 'liqm_review_display_comment_text' ) ) {
	function liqm_review_display_comment_text() {
		comment_text();
	}
}
