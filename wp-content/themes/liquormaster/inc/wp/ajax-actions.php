<?php
/**
 * Collection of theme-specific ajax actions
 *
 * @package WordPress
 */

ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );


/**
 * Handler of ajax-call to get Privat Bank Currency Exchange Archive.
 */
add_action( 'wp_ajax_get_privat_bank_archive_currency_exchange', 'get_privat_bank_archive_currency_exchange' );
add_action( 'wp_ajax_nopriv_get_privat_bank_archive_currency_exchange', 'get_privat_bank_archive_currency_exchange' );

function get_privat_bank_archive_currency_exchange() {

	$date   = $_GET['date'];
	$date   = date( 'd.m.Y', strtotime( $date ) );
	$output = file_get_contents( 'https://api.privatbank.ua/p24api/exchange_rates?json&date=' . $date );

	$output = json_decode( $output );
	wp_send_json( $output );
}

/**
 * Handler of ajax-call to get Privat Bank Currency Exchange.
 */
add_action( 'wp_ajax_get_privat_bank_currency_exchange_data', 'get_privat_bank_currency_exchange_data' );
add_action( 'wp_ajax_nopriv_get_privat_bank_currency_exchange_data', 'get_privat_bank_currency_exchange_data' );

function get_privat_bank_currency_exchange_data() {

	$output = file_get_contents( 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5' );

	$output = json_decode( $output );

	wp_send_json( $output );
}

/**
 * Handler of ajax-call to get list of countries reported COVID-19 spreading.
 */
add_action( 'wp_ajax_get_covid_countries_data', 'get_covid_countries_data' );
add_action( 'wp_ajax_nopriv_get_covid_countries_data', 'get_covid_countries_data' );

function get_covid_countries_data() {

	$result = get_transient( 'covid_countries_list_' . ICL_LANGUAGE_CODE );

	if ( empty( $result ) ) {
		$fetch = wp_remote_get( 'https://api.covid19api.com/countries' );

		if ( $fetch['response']['code'] !== 404 && $fetch !== null ) {
			$countries_list = json_decode( $fetch['body'] );
			$result         = null;

			foreach ( $countries_list as $index => $country ) {
				$country_name = _x( $country->Country, 'country name', 'liquormaster' );
				$result       .= '<option value="' . $country->ISO2 . '">' . $country_name . '</option>';
			}

			set_transient( 'covid_countries_list_' . ICL_LANGUAGE_CODE, $result, 3600 );
			wp_send_json( $result );
		} else {
			wp_send_json( $fetch['response'] );
		}
	} else {
		wp_send_json( $result );
	}
}

/**
 * Handler of ajax-call to get COVID-19 spreading info for specific country.
 */
add_action( 'wp_ajax_get_covid_country_data', 'get_covid_country_data' );
add_action( 'wp_ajax_nopriv_get_covid_country_data', 'get_covid_country_data' );

function get_covid_country_data() {

	$country_name = $_GET['country'];

	$result = get_transient( 'covid_' . $country_name . '_' . ICL_LANGUAGE_CODE );

	if ( empty( $result ) ) {
		$fetch = wp_remote_get( 'https://api.covid19api.com/total/country/' . $country_name );

		if ( $fetch['response']['code'] !== 404 ) {
			$country = json_decode( $fetch['body'] );
			$result  = '';
			if ( empty( $country ) ) {
				wp_send_json( $result );
			} else {
				$chart  = array();
				$result = array( 'table_data' => null, 'chart_data' => null );

				$chart['data_labels']['active']    = _x( 'Active', 'chart data labels', 'liquormaster' );
				$chart['data_labels']['сonfirmed'] = _x( 'Confirmed', 'chart data labels', 'liquormaster' );
				$chart['data_labels']['recovered'] = _x( 'Recovered', 'chart data labels', 'liquormaster' );
				$chart['data_labels']['deaths']    = _x( 'Deaths', 'chart data labels', 'liquormaster' );
				foreach ( $country as $key => $country_data ) {
					$timestamp                                = strtotime( $country_data->Date );
					$date                                     = date( 'd.m.Y', $timestamp );
					$month                                    = date( 'F Y', $timestamp );
					$translation_month                        = date( 'F', $timestamp );
					$year                                     = date( 'Y', $timestamp );
					$chart['labels'][]                        = _x( $translation_month, 'months list', 'liquormaster' ) . ' ' . $year;
					$chart['months'][ $month ]['active'][]    = $country_data->Active;
					$chart['months'][ $month ]['confirmed'][] = $country_data->Confirmed;
					$chart['months'][ $month ]['recovered'][] = $country_data->Recovered;
					$chart['months'][ $month ]['deaths'][]    = $country_data->Deaths;
					$result['table_data']                     .= <<<HTML
<tr>
    <td>{$date}</td>
    <td>{$country_data->Active}</td>
    <td>{$country_data->Confirmed}</td>
    <td>{$country_data->Recovered}</td>
    <td>{$country_data->Deaths}</td>
</tr>
HTML;
				}

				$chart['labels'] = array_values( array_unique( $chart['labels'] ) );
				foreach ( $chart['months'] as $key => $month ) {
					$chart['data']['active'][]    = end( $month['active'] );
					$chart['data']['confirmed'][] = end( $month['confirmed'] );
					$chart['data']['recovered'][] = end( $month['recovered'] );
					$chart['data']['deaths'][]    = end( $month['deaths'] );
					unset( $chart['months'][ $key ] );
				}

				unset( $chart['months'] );

				$result['chart_data'] = $chart;

				set_transient( 'covid_' . $country_name . '_' . ICL_LANGUAGE_CODE, $result, 3600 );
				wp_send_json( $result );
			}
		} else {
			wp_send_json( $fetch['response'] );
		}
	} else {
		wp_send_json( $result );
	}
}

/**
 * Handler of ajax-call to get FBI fugitives list for specific field office.
 */
add_action( 'wp_ajax_get_fbi_wanted_list', 'get_fbi_wanted_list' );
add_action( 'wp_ajax_nopriv_get_fbi_wanted_list', 'get_fbi_wanted_list' );

function get_fbi_wanted_list() {

	$field_office = $_GET['field_office'];
	$current_page = ( isset( $_GET['page'] ) && ! empty( $_GET['page'] ) ) ? $_GET['page'] : 1;
	$result       = get_transient( 'fbi_data_' . $field_office . '_' . $current_page . '_' . ICL_LANGUAGE_CODE );

	if ( empty( $result ) ) {


		$fetch = wp_remote_get( 'https://api.fbi.gov/@wanted?pageSize=12&page=' . $current_page . '&sort_on=modified&sort_order=desc&field_offices=' . $field_office );

		if ( 404 !== $fetch['response']['code'] ) {

			$wanted_list = json_decode( $fetch['body'] );

			$result              = array();
			$pages_count         = ceil( $wanted_list->total / 12 );
			$result['items']     = '';
			$no_reward_text      = _x( 'No current reward announced', 'fbi-wanted', 'liquormaster' );
			$no_description_text = _x( 'No current description', 'fbi-wanted', 'liquormaster' );
			foreach ( $wanted_list->items as $index => $wanted_fugitive ) {

				$reward_text     = ( ! empty( $wanted_fugitive->reward_text ) ) ? $wanted_fugitive->reward_text : $no_reward_text;
				$description     = ( ! empty( $wanted_fugitive->description ) ) ? $wanted_fugitive->description : $no_description_text;
				$result['items'] .= <<<HTML
<li class="fugitive-info">
    <div class="fugitive-photo">
    	<a href="{$wanted_fugitive->url}" target="_blank">
        	<img src="{$wanted_fugitive->images[0]->original}" alt="">
    	</a>
    </div>
    <div class="fugitive-name">
	    <a href="{$wanted_fugitive->url}" target="_blank">
			{$wanted_fugitive->title}
		</a>
	</div>
    <div class="fugitive-reward">{$reward_text}</div>
    <div class="fugitive-description">{$description}</div>
</li>
HTML;
			}

			$result['pages'] = '';

			if ( $pages_count < 2 ) {
				$result['pages'] = null;
			} else {
				for ( $i = 1; $i <= $pages_count; $i ++ ) {
					$current         = ( $i == 1 ) ? 'current' : '';
					$result['pages'] .= '<li class="' . $current . '" data-page="' . $i . '"><span>' . $i . '</span></li>';
				}
			}

			set_transient( 'fbi_data_' . $field_office . '_' . $current_page . '_' . ICL_LANGUAGE_CODE, $result, 18000 );
			wp_send_json( $result );
		} else {
			wp_send_json( $fetch['response'] );
		}
	} else {
		wp_send_json( $result );
	}
}

/**
 * Handler of ajax-call to get list of specific alcohol category.
 */
add_action( 'wp_ajax_filter_products_with_dropdown', 'filter_products_with_dropdown' );
add_action( 'wp_ajax_nopriv_filter_products_with_dropdown', 'filter_products_with_dropdown' );

if ( ! function_exists( 'filter_products_with_dropdown' ) ) {
	function filter_products_with_dropdown() {

		$liquor_categories = explode( ',', esc_sql( $_GET['liquor'] ) );

		$posts_per_page = ( wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() );
		$page_number    = ( isset( $_GET['page-number'] ) && ! empty( intval( $_GET['page-number'] ) ) ) ? ( $_GET['page-number'] - 1 ) : 0;


		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => $posts_per_page,
			'offset'         => ( $posts_per_page * $page_number ),
			'orderby'        => 'id',
			'order'          => 'ASC',
			'tax_query'      => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $liquor_categories,
					'operator' => 'IN',
				),
			),
		);

		$filtered_products = new WP_Query( $args );

		if ( $filtered_products->have_posts() ) {
			$result = array(
				'products' => '',
				'pages'    => '',
			);
			while ( $filtered_products->have_posts() ) {
				$filtered_products->the_post();
				ob_start();
				wc_get_template_part( 'content', 'product' );
				$result['products'] .= ob_get_clean();
			}
		}

		if ( $filtered_products->max_num_pages < 2 ) {
			$result['pages'] = null;
		} else {
			for ( $i = 1; $i <= $filtered_products->max_num_pages; $i ++ ) {
				$current         = ( $i == ( $page_number + 1 ) ) ? 'current' : '';
				$result['pages'] .= '<li class="ajax-pagination" data-page="' . $i . '"><span class="' . $current . ' page-numbers">' . $i . '</span></li>';
			}
		}
		wp_send_json( $result );
	}
}

/**
 * Handler of ajax-call to get count of products in wishlist .
 */
add_action( 'wp_ajax_get_wishlist_count', 'get_wishlist_count' );
add_action( 'wp_ajax_nopriv_get_wishlist_count', 'get_wishlist_count' );

if ( ! function_exists( 'get_wishlist_count' ) ) {
	function get_wishlist_count() {
		$result = array();

		$result['wishlist_url']   = YITH_WCWL()->get_wishlist_url() . '=view&wishlist_id=' . YITH_WCWL()->get_current_user_wishlists()[0]->get_token();
		$result['wishlist_count'] = yith_wcwl_count_all_products();

		wp_send_json( $result );
	}
}

/**
 * Handler of ajax-call to get count of products for comparison.
 */
add_action( 'wp_ajax_get_compare_count', 'get_compare_count' );
add_action( 'wp_ajax_nopriv_get_compare_count', 'get_compare_count' );

if ( ! function_exists( 'get_compare_count' ) ) {
	function get_compare_count() {
		$result = array();

		$compared_products       = $_COOKIE['yith_woocompare_list'];
		$compared_products_count = count( json_decode( $compared_products ) );

		$result['compare_count'] = $compared_products_count;

		wp_send_json( $result );

	}
}


add_action( 'wp_ajax_remove_checked_products_from_cart', 'remove_checked_products_from_cart' );
add_action( 'wp_ajax_nopriv_remove_checked_products_from_cart', 'remove_checked_products_from_cart' );

if ( ! function_exists( 'remove_checked_products_from_cart' ) ) {
	function remove_checked_products_from_cart() {
		if ( isset( $_POST['products_list'] ) && ! empty( $_POST['products_list'] ) ) {
			$product_ids = explode(',', $_POST['products_list'] );

			foreach ( $product_ids as $product_id ) {
				$product_cart_id = WC()->cart->generate_cart_id( $product_id );
				$cart_item_key   = WC()->cart->find_product_in_cart( $product_cart_id );
				if ( $cart_item_key ) {
					WC()->cart->remove_cart_item( $cart_item_key );
				};
			}

			wp_send_json_success();
		}

		return false;
	}
}
