<?php
/**
 * Place to register navigation menus.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

if ( ! function_exists( 'liqm_register_nav_menu' ) ) {

	/**
	 * Function to register menus
	 */
	function liqm_register_nav_menu() {
		register_nav_menus(
			array(
				'primary_menu'        => __( 'Primary Menu', 'text_domain' ),
				'account_links'       => __( 'Account Links', 'text_domain' ),
				'informational_links' => __( 'Informational Links', 'text_domain' ),
				'quick_links'         => __( 'Quick Links', 'text_domain' ),
				'account_menu'        => __( 'Account menu', 'text_domain' ),
			)
		);
	}

	add_action( 'after_setup_theme', 'liqm_register_nav_menu', 0 );
}
