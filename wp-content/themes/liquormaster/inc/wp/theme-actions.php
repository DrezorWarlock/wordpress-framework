<?php
/**
 * Collection of theme-specific actions
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

add_action( 'pre_get_posts', 'liqm_get_all_team_members' );

if ( ! function_exists( 'liqm_get_all_team_members' ) ) {
	/**
	 * Function to get all team members data to render.
	 *
	 * @param object $query DB Query.
	 */
	function liqm_get_all_team_members( $query ) {
		if ( $query->is_main_query() && is_post_type_archive( 'team' ) ) {
			$query->set( 'posts_per_page', - 1 );
		}
	}
}

add_action( 'acf/init', 'liqm_acf_google_maps_init' );

if ( ! function_exists( 'liqm_acf_google_maps_init' ) ) {
	/**
	 * Function to provide google maps key for ACF Google Map
	 */
	function liqm_acf_google_maps_init() {
		acf_update_setting( 'google_api_key', get_field( 'google_map_api_key', 'option' ) );
	}
}
