<?php
/**
 * Yoast's filters
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

add_filter( 'wpseo_breadcrumb_single_link', 'liqm_remove_last_breadcrumb' );

if ( ! function_exists( 'liqm_remove_last_breadcrumb' ) ) {
	/**
	 * Function to remove last breadcrumbs element
	 *
	 * @param string $link current breadcrumb element.
	 *
	 * @return mixed|string
	 */
	function liqm_remove_last_breadcrumb( $link ): string {
		if ( strpos( $link, 'breadcrumb_last' ) !== false ) {
			$link = '';
		}

		return $link;
	}
}
