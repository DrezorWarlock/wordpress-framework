<?php
/**
 * Collection of theme-specific filters
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

add_filter( 'nav_menu_css_class', 'liqm_add_menu_item_custom_classes', 10, 4 );

if ( ! function_exists( 'liqm_add_menu_item_custom_classes' ) ) {
	/**
	 * Function to filter menu items. Filter aim is set by developer.
	 *
	 * @param array    $classes list of menu item classes.
	 * @param WP_Post  $item object holding current menu item data.
	 * @param stdClass $args object holding list of arguments set in wp_nav_menu() function call.
	 * @param integer  $depth depth of menu submenus.
	 *
	 * @return mixed|array
	 */
	function liqm_add_menu_item_custom_classes( array $classes, $item, stdClass $args, int $depth ): array {
		if ( 'primary_menu' === $args->theme_location ) {
			$classes[] = 'nav-item';
			if ( in_array( 'current-menu-item', $classes ) ) {
				$classes[] = 'active';
			}

			if ( in_array( 'menu-item-has-children', $classes ) ) {
				$classes[] = 'dropdown';
			}
		}

		return $classes;
	}
}

add_filter( 'nav_menu_link_attributes', 'liqm_add_menu_item_link_custom_classes', 10, 4 );

if ( ! function_exists( 'liqm_add_menu_item_link_custom_classes' ) ) {
	/**
	 * Function to filter menu item link. Filter aim is set by developer.
	 *
	 * @param array    $atts array with attributes of menu item link.
	 * @param WP_Post  $item object holding current menu item data.
	 * @param stdClass $args object holding list of arguments set in wp_nav_menu() function call.
	 * @param integer  $depth depth of item link.
	 *
	 * @return mixed|array
	 */
	function liqm_add_menu_item_link_custom_classes( array $atts, $item, stdClass $args, int $depth ): array {
		if ( 'primary_menu' === $args->theme_location ) {

			if ( in_array( 'menu-item-has-children', $item->classes ) ) {
				$atts['class'] = 'nav-link dropdown-toggle';
			} else {
				$atts['class'] = 'nav-link';
			}
		}

		if ( 1 === $depth ) {
			$atts['class'] = 'dropdown-item';
		}

		return $atts;
	}
}

add_filter( 'nav_menu_submenu_css_class', 'liqm_add_submenu_custom_classes', 10, 2 );

if ( ! function_exists( 'liqm_add_submenu_custom_classes' ) ) {
	/**
	 * Function to filter menu item link. Filter aim is set by developer.
	 *
	 * @param array    $classes array with attributes of menu item link.
	 * @param stdClass $args object holding list of arguments set in wp_nav_menu() function call.
	 *
	 * @return mixed|array
	 */
	function liqm_add_submenu_custom_classes( array $classes, stdClass $args ): array {
		if ( 'primary_menu' === $args->theme_location ) {
			$classes[] = 'dropdown-menu';
		}

		return $classes;
	}
}

add_filter( 'upload_mimes', 'liqm_add_webp_mime_type_support', 100, 1 );

if ( ! function_exists( 'liqm_add_webp_mime_type_support' ) ) {
	/**
	 * Function to add .webp extension support to WP.
	 *
	 * @param array $mime_types list mine-types supported.
	 *
	 * @return array|mixed
	 */
	function liqm_add_webp_mime_type_support( array $mime_types ): array {
		$mime_types['webp'] = 'image/webp';

		return $mime_types;
	}
}

add_filter( 'wp_list_categories', 'liqm_custom_categories_listing_html', 100, 2 );

if ( ! function_exists( 'liqm_custom_categories_listing_html' ) ) {
	/**
	 * Function to return custom categories listing output.
	 *
	 * @param string $output html output of categories listing.
	 * @param array  $args arguments of categories listing.
	 *
	 * @return string
	 */
	function liqm_custom_categories_listing_html( $output, $args ) {
		$output    = '<div class="categories">';
		$item_icon = get_field( 'link_item_icon', 'option' );
		if ( 'service_categories' === $args['taxonomy'] ) :
			$terms = get_terms( $args['taxonomy'] );
			foreach ( $terms as $term ) :
				$term_link = get_term_link( $term->term_id );
				$output    .= <<<HTML
<li><a href="{$term_link}">$term->name <span class="fa {$item_icon}"></span></a></li>
HTML;
			endforeach;
			$output .= '</div>';

			return $output;
		endif;
	}
}

add_filter( 'widget_tag_cloud_args', 'liqm_set_widget_tag_cloud_args' );

if ( ! function_exists( 'liqm_set_widget_tag_cloud_args' ) ) {
	/**
	 * Function to set tag cloud arguments.
	 *
	 * @param array $args array of tag cloud arguments.
	 *
	 * @return array
	 */
	function liqm_set_widget_tag_cloud_args( array $args ): array {
		$args['hide_empty'] = false;

		return $args;
	}
}

add_filter( 'the_title', 'liqm_remove_private_prefix' );

if ( ! function_exists( 'liqm_remove_private_prefix' ) ) {
	/**
	 * @param string $title title of the page/post.
	 *
	 * @return string|string[]
	 */
	function liqm_remove_private_prefix( string $title ): string {

		$str_to_replace = _x( 'Private: ', 'page 404', 'liquormaster' );
		$title          = str_replace( $str_to_replace, '', $title );

		return $title;
	}
}

add_filter( 'widget_tag_cloud_args', 'ligm_custom_tag_cloud_widget' );

if ( ! function_exists( 'ligm_custom_tag_cloud_widget' ) ) {
	/**
	 * Function to set the smallest and biggest possible font-size of the tag cloud elements.
	 *
	 * @param array $args array of tag cloud arguments.
	 *
	 * @return mixed
	 */
	function ligm_custom_tag_cloud_widget( array $args ): array {
		$args['largest']  = 11;
		$args['smallest'] = 11;
		$args['unit']     = 'px';

		return $args;
	}
}

add_filter( 'get_the_archive_title_prefix', 'liqm_remove_archive_prefix', 100, 1 );

if ( ! function_exists( 'liqm_remove_archive_prefix' ) ) {
	/**
	 * Function to remove archive title prefix.
	 *
	 * @param string $prefix archive title prefix.
	 *
	 * @return string
	 */
	function liqm_remove_archive_prefix( string $prefix ): string {
		$prefix = '';

		return $prefix;
	}
}

add_filter( 'acf/load_field/name=post_type_recent_blog', 'acf_load_post_types' );

if ( ! function_exists( 'acf_load_post_types' ) ) {
	/**
	 * Function to populate select for post_type_recent_blog in admin panel.
	 *
	 * @param array $field list of choices for post_type_recent_blog ACF.
	 *
	 * @return mixed
	 */
	function acf_load_post_types( $field ) {
		foreach ( get_post_types( array( 'publicly_queryable' => 1 ), 'names' ) as $post_type ) {
			$field['choices'][ $post_type ] = $post_type;
		}

		return $field;
	}
}

add_filter( 'comment_form_fields', 'liqm_move_comment_field_to_bottom' );

if ( ! function_exists( 'liqm_move_comment_field_to_bottom' ) ) {
	/**
	 * Function to move comment textarea to the bottom of the form.
	 *
	 * @param array $fields list of comment form fields.
	 *
	 * @return mixed
	 */
	function liqm_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;

		return $fields;
	}
}
