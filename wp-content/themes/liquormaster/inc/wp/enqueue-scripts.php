<?php
/**
 * Theme CSS & JS
 */
function theme_scripts() {
	// Main CSS.
	wp_enqueue_style( 'main-stylesheet', asset_path( 'styles/main.css' ), false, '1.0.0' );

	// Google API fonts.
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Spectral:ital,wght@0,200;0,300;0,400;0,500;0,700;0,800;1,200;1,300;1,400;1,500;1,700&display=swap', false, null );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery.
	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', [], '2.2.4', false );

	// Main JS.
	wp_enqueue_script( 'main-javascript', asset_path( 'scripts/main.js' ), [ 'jquery' ], '1.0.0', true );
	wp_enqueue_script( 'comment-reply' );

	// Throw variables to front-end.
	$theme_vars = array(
		'home'    => get_home_url(),
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'is_auth' => is_user_logged_in(),
	);
	wp_localize_script( 'main-javascript', 'themeVars', $theme_vars );
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

if ( ! function_exists( 'load_admin_styles' ) ) {
	function load_admin_styles() {
		wp_enqueue_style('admin-css', THEME_DIR_URI.'/assets/styles/admin.css');
	}
}

add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
