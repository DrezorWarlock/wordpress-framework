<?php
/**
 * File for implementation of acf-based option pages
 *
 * @package WordPress
 */

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => __( 'Theme General Settings', 'liquormaster' ),
			'menu_title' => __( 'Theme Settings', 'liquormaster' ),
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
}
