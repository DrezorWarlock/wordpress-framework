<?php
/**
 * Theme's search template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>

<section class="ftco-section ftco-no-pb">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="search-intro-text">
					<h2>
						<?php echo _x( 'Here\'s what we\'ve managed to find for you', 'search_page', 'liquormaster' ); ?>
					</h2>
				</div>
				<div class="search-results">
					<?php echo get_search_query( true ); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
?>

