<?php
/**
 * Theme's home template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();

require_once 'inc/template-classes/class-liqmteam.php';

$team_archive = new LiqmTeam();

$team_archive->render();
get_footer();
