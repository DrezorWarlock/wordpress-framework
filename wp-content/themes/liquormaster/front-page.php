<?php
/**
 * Theme's front-page template
 *
 * @package WordPress
 */

defined('ABSPATH') || die('Iwanu ga hana');

get_header();

require_once 'inc/template-classes/class-liqmfrontpage.php';

$front_page = new LiqmFrontPage();

$front_page->render();

get_footer();
?>

