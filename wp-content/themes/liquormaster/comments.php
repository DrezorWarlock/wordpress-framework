<?php
/**
 * Template for comments list.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );


?>

<div class="pt-5 mt-5">
	<h3 class="mb-5">
		<?php
		$titles = array(
				__('%d comment', 'liquormaster'),
				_x('%d comments', '2-4', 'liquormaster'),
				__('%d comments', 'liquormaster'),
		);
		echo declOfNum(get_comments_number(), $titles);
//		sprintf(_n('%d comment', '%d comments', get_comments_number(), 'liquormaster' ), get_comments_number()); ?>
	</h3>
	<ul class="comment-list">
		<?php
		wp_list_comments(
			array(
				'walker' => new Liqm_Comments_Walker(),
				'type'   => 'comment',
			)
		);
		?>
	</ul>
	<?php
	comment_form(
		array(
			'class_container'      => 'comment-form-wrap comment-respond',
			'class_form'           => 'p-5 bg-light comment-form',
			'title_reply_before'   => '<h3 id="reply-title" class="	comment-reply-title">',
			'title_reply'          => _x('Leave a comment', 'comment form', 'liquormaster'),
			'title_reply_after'    => '</h3>',
			'comment_notes_before' => '',
			'submit_button'        => '<div class="form-group">
										<input type="submit" value="' . _x('Post Comment', 'comment form', 'liquormaster') . '" class="btn py-3 px-4 btn-primary">
									</div>',
			'comment_field'        => '<div class="form-group">
										<label for="message">' . _x('Message', 'comment form', 'liquormaster') . '</label>
										<textarea name="comment" id="message" cols="30" rows="10" maxlength="65525" class="form-control" required="required"></textarea>
									</div>',
			'fields'               => array(
				'author' => '<div class="form-group">
								<label for="author">'. _x('Name', 'comment form', 'liquormaster') . '*</label>
								<input type="text" name="author" class="form-control" maxlength="245" id="author" required="required">
							</div>',
				'email'  => '<div class="form-group">
								<label for="email">' . _x('Email', 'comment form', 'liquormaster') . '*</label>
								<input type="email" name="email" aria-describedby="email-notes" maxlength="100" class="form-control" id="email" required="required">
							</div>',
				'url'    => '<div class="form-group">
								<label for="website">'. _x('Website', 'comment_form', 'liquormaster') . '</label>
								<input type="url" name="url" class="form-control" maxlength="200" id="website">
							</div>',
			),
		)
	);
	?>
</div>
