<?php
define( 'THEME_DIR', get_template_directory() );
define( 'THEME_DIR_URI', get_template_directory_uri() );

require_once THEME_DIR . '/inc/helpers/file.php';
require_once THEME_DIR . '/inc/helpers/theme-helpers.php';
require_once THEME_DIR . '/inc/helpers/numerals.php';
require_once THEME_DIR . '/inc/helpers/static-translations.php';
require_once THEME_DIR . '/inc/custom-walkers/class-liqm-footer-nav-menu-walker.php';
require_once THEME_DIR . '/inc/custom-walkers/class-liqm-comments-walker.php';
require_once THEME_DIR . '/inc/wp/enqueue-scripts.php';
require_once THEME_DIR . '/inc/wp/theme-supports.php';
require_once THEME_DIR . '/inc/wp/theme-actions.php';
require_once THEME_DIR . '/inc/wp/theme-filters.php';
require_once THEME_DIR . '/inc/wp/yoast-filters.php';
require_once THEME_DIR . '/inc/wp/acf-options-page.php';
require_once THEME_DIR . '/inc/wp/menus-register.php';
require_once THEME_DIR . '/inc/wp/sidebars-collection.php';
require_once THEME_DIR . '/inc/wp/shortcodes-collection.php';
require_once THEME_DIR . '/inc/wp/ajax-actions.php';
require_once THEME_DIR . '/inc/wp/woocommerce-filters.php';
require_once THEME_DIR . '/inc/wp/woocommerce-actions.php';
require_once THEME_DIR . '/inc/wp/woocommerce-removes.php';
