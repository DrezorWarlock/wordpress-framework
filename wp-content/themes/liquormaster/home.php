<?php
/**
 * Theme's home template
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

get_header();
?>

	<section class="ftco-section">
		<div class="container">
			<div class="row d-flex">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post()
						?>
						<div class="col-lg-6 d-flex align-items-stretch ftco-animate">
							<div class="blog-entry d-md-flex">
								<a href="<?php the_permalink(); ?>" class="block-20 img"
								   style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
								</a>
								<div class="text p-4 bg-light">
									<div class="meta">
										<p><span class="fa <?php the_field('blog_icons_blog_icon_calendar', 'option'); ?>"></span><?php echo get_the_date( 'j F Y' ); ?>
										</p>
									</div>
									<h3 class="heading mb-3">
										<a href="<?php the_permalink(); ?>">
											<?php the_title(); ?>
										</a>
									</h3>
									<p><?php the_excerpt(); ?></p>
									<a href="<?php the_permalink(); ?>" class="btn-custom">
										<?php echo _x('Continue', 'archives', 'liquormaster');?>
										<span
												class="fa <?php the_field('view_further_icon', 'option'); ?>"></span></a>
								</div>
							</div>
						</div>
					<?php
					endwhile;
				else :
					?>
					<div class="col-lg-12 d-flex justify-content-center align-items-stretch ftco-animate">
						<h3>
							<?php _ex( 'No posts found!', 'archive no posts found', 'liquormaster' ); ?>
						</h3>
					</div>
					<?php
				endif;
				?>
			</div>
			<div class="row mt-5">
				<div class="col text-center">
					<div class="block-27">
						<?php
						echo paginate_links(
								array(
										'prev_text' => '<',
										'next_text' => '>',
										'type'      => 'list',
								)
						);
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
