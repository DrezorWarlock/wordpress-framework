<?php
/**
 * Site pages hero section
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$blog_title = ( is_home() ) ? get_option( 'page_for_posts' ) : null;
?>
<section class="hero-wrap hero-wrap-2"
		 style="background-image: url('<?php echo THEME_DIR_URI ?>/assets/images/bg_2.jpg');"
		 data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate mb-5 text-center">
				<?php yoast_breadcrumb( '<p class="breadcrumbs mb-0">', '</p>' ) ?>
				<h2 class="mb-0 bread"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
</section>
