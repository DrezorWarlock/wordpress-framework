<?php

/**
 * Template part for main shop page
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'Iwanu ga hana' );

$args = array(
		'parent'  => 0,
		'exclude' => 29
);

$select_options = get_terms( 'product_cat', $args );
?>

<section class="ftco-section">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="row mb-4">
					<div class="col-md-12 d-flex justify-content-between align-items-center">
						<h4 class="product-select">
							<?php echo _x( 'Select Types of Products', 'shop', 'liquormaster' ); ?>
						</h4>
						<select class="selectpicker" multiple>
							<?php foreach ( $select_options as $select_option ) : ?>
								<option value="<?php echo $select_option->term_id ?>"><?php echo $select_option->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<?php
				woocommerce_content();
				?>
			</div>
			<div class="col-md-3">
				<?php dynamic_sidebar( 'wc_shop_sidebar' ); ?>
			</div>
		</div>
	</div>
</section>
