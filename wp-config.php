<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

global $environments;
require_once 'credentials.php';

$environment = ( preg_match( '/\.loc/', $_SERVER['HTTP_HOST'] ) ) ? 'dev' : 'prod';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', $environments[$environment]['DB_NAME'] );

/** MySQL database username */
define( 'DB_USER', $environments[$environment]['DB_USER'] );

/** MySQL database password */
define( 'DB_PASSWORD', $environments[$environment]['DB_PASS'] );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(g9JW9(ATe5sLMWATq>)RFx=u|9eeBlS?mA =`^NW/Bz$!U2MG2P-Z.nQZiu&hTA' );
define( 'SECURE_AUTH_KEY',  'pFXbu{qM:i 5{=LW=+_VO!KVtQ Ep.->?X#Am*(P]71GCU+|?Oco590%)uU]l/&a' );
define( 'LOGGED_IN_KEY',    '~ahi_KyFvof7u-yKTfQ3/=! 7V5ykJRO2;B<vF!1K}=BbCF.:rr|j830]()mV[!]' );
define( 'NONCE_KEY',        'Ez.Mds9?BbPF)sS>%|pUj5#i7&#{;*9pTv9@Mqd7c6Z*}JJhew1.-[RlNN(]h}|B' );
define( 'AUTH_SALT',        '5Opy)ALQzG=^0BZC}tc0s*TVrZ/:+n/|`qMs|4_*pR*[~jy_)[8H/o&V#{`<3MY0' );
define( 'SECURE_AUTH_SALT', '^#,o3y0REde8~T$shwT*%Y}jz9xu4KMWHxmjBS,Ot4MC8B,WyB)A0rX,R>%or%|K' );
define( 'LOGGED_IN_SALT',   '4@W(-0oi)AY2_!|N!p5EB~Z1C! vBB%zTN^eNi?Hh+?h%?7N3#HTv1qRRb)O!;%S' );
define( 'NONCE_SALT',       ')G}FH}c,eHuyhpUE@O1ZV-dK[k<~nR@%8oj|/s:P${t@~>&BB*,qF/]NvohS&df5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'liq_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', $environments[$environment]['DEBUG'] );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
