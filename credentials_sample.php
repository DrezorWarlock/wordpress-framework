<?php
/**
 * Credentials for different environments
 *
 * @package WordPress
 */

global $environments;

$environments = array(
	'dev'  => array(
		'DB_NAME' => '',
		'DB_USER' => '',
		'DB_PASS' => '',
		'DB_HOST' => '',
		'DEBUG'   => true,
	),
	'prod' => array(
		'DB_NAME' => '',
		'DB_USER' => '',
		'DB_PASS' => '',
		'DB_HOST' => '',
		'DEBUG'   => false,
	),
);

return $environments;

